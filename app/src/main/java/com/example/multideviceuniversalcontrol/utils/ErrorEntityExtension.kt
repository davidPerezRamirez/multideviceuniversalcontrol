package com.example.multideviceuniversalcontrol.utils

import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection
import java.util.concurrent.TimeoutException

fun Throwable.toErrorEntity(): ErrorEntity {
    val errorEntity = when (this) {
        is IOException -> ErrorEntity.Network(this)

        is TimeoutException -> ErrorEntity.Network(this)

        is java.net.UnknownHostException -> ErrorEntity.Network(this)

        is HttpException -> {
            when (this.code()) {

                // access denied
                HttpURLConnection.HTTP_FORBIDDEN -> ErrorEntity.AccessDenied(this)

                // not found
                in HttpURLConnection.HTTP_BAD_REQUEST..HttpURLConnection.HTTP_NOT_FOUND ->
                    ErrorEntity.NotFound(this)

                // unavailable service
                in HttpURLConnection.HTTP_INTERNAL_ERROR..HttpURLConnection.HTTP_VERSION ->
                    ErrorEntity.ServiceUnavailable(this)

                // all the others will be treated as unknown error
                else -> ErrorEntity.Unknown(this)
            }
        }
        else -> null
    }
    return if (errorEntity == null && cause != null) {
        cause!!.toErrorEntity()
    } else errorEntity ?: ErrorEntity.Unknown(this)
}