package com.example.multideviceuniversalcontrol.utils

import android.view.View
import androidx.annotation.IdRes

fun <V : View> View.lazyFindViewById(@IdRes res: Int): Lazy<V> {
    return lazy(LazyThreadSafetyMode.NONE) {
        this.findViewById<V>(res)
    }
}