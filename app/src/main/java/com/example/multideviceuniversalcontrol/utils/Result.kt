package com.example.multideviceuniversalcontrol.utils

sealed class Result<T> {

    data class Success<T>(val data: T) : Result<T>()

    data class Error<T>(val error: ErrorEntity) : Result<T>()
}

sealed class ErrorEntity {

    abstract val originalException: Throwable

    data class Network(override val originalException: Throwable) : ErrorEntity()

    data class NotFound(override val originalException: Throwable) : ErrorEntity()

    data class AccessDenied(override val originalException: Throwable) : ErrorEntity()

    data class ServiceUnavailable(override val originalException: Throwable) : ErrorEntity()

    data class Unknown(override val originalException: Throwable) : ErrorEntity()

    abstract class ExternalErrorEntity : ErrorEntity()
}