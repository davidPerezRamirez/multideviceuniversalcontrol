package com.example.multideviceuniversalcontrol.utils

import android.app.Activity
import android.view.View
import androidx.annotation.IdRes

fun <V : View> Activity.lazyFindViewById(@IdRes res: Int): Lazy<V> {
    return lazy(LazyThreadSafetyMode.NONE) {
        this.findViewById(res)
    }
}