package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonTypeDeviceRelation

object ButtonTypeDeviceRelationConfig {

    fun create(): ArrayList<ButtonTypeDeviceRelation> {
        return arrayListOf(
            /** Button to Television  **/
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 1, code = "0x20DF10EF"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 2, code = "0x20DF40BF"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 3, code = "0x20DFC03F"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 4, code = "0x20DF00FF"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 5, code = "0x20DF807F"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 6, code = "0x20DF906F"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 7, code = "0x20DF14EB"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 8, code = "0x20DF08F7"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 9, code = "0x20DF8877"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 10, code = "0x20DF48B7"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 11, code = "0x20DFC837"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 12, code = "0x20DF28D7"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 13, code = "0x20DFA857"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 14, code = "0x20DF6897"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 15, code = "0x20DFE817"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 16, code = "0x20DF18E7"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 18, code = "0x20DF0DF2"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 19, code = "0x20DF5DA2"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 20, code = "0x20DF02FD"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 21, code = "0x20DF827D"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 22, code = "0x20DF22DD"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 23, code = "0x20DF609F"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 24, code = "0x20DFE01F"),
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 25, code = "0x20DF3EC1"),

            /** Button to Audio System  **/
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 1, code = "0xFF827D"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 2, code = "0xFF609F"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 3, code = "0xFF926D"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 6, code = "0xFF827D"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 26, code = "0xFFE01F"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 27, code = "0xFF3AC5"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 28, code = "0xFF906F"),
            ButtonTypeDeviceRelation(idDevice = 2, idDeviceButton = 29, code = "0xFF7887")
        )
    }
}