package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.*

@Entity(
    indices = [Index(value = ["idDevice", "idDeviceButton"], unique = true)],
    foreignKeys = [ForeignKey(
        entity = DeviceButton::class,
        parentColumns = ["idDeviceButton"],
        childColumns = ["idDeviceButton"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    ), ForeignKey(
        entity = Device::class,
        parentColumns = ["idDevice"],
        childColumns = ["idDevice"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )]
)
data class ButtonTypeDeviceRelation(

    @PrimaryKey(autoGenerate = true) val idButtonTypeDeviceRelation: Int = 0,
    @ColumnInfo(name = "idDeviceButton") val idDeviceButton: Int,
    @ColumnInfo(name = "idDevice") val idDevice: Int,
    @ColumnInfo(name = "code") val code: String

)