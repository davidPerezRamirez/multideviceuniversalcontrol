package com.example.multideviceuniversalcontrol.infraestructure.repository

import com.example.multideviceuniversalcontrol.domain.network.contracts.DataBaseRepository
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class DataBaseRepositoryImpl: DataBaseRepository {

    override suspend fun getDeviceTypesEnabled() = coroutineScope {
        val devices = AppDatabase.INSTANCE!!.deviceRepository().getAll()

        val deviceTypesDeferred = mutableSetOf<Deferred<DeviceType>>()
        devices.forEach {
            deviceTypesDeferred.add(
                async {
                    AppDatabase.INSTANCE!!.deviceTypeRepository().findById(it.idDeviceType)
                }
            )
        }
        deviceTypesDeferred
            .map { it.await() }
            .toList()
            .distinctBy { it.idDeviceType }
            .sortedBy { it.idDeviceType }

    }
}