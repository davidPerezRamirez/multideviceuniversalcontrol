package com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm

import com.example.multideviceuniversalcontrol.domain.User
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.StatusConnection
import com.example.multideviceuniversalcontrol.domain.StatusConnectionWifi
import com.example.multideviceuniversalcontrol.domain.arduino.model.StatusConnectionResponse
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.WebServerRepository
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.toErrorEntity
import kotlinx.coroutines.coroutineScope

class WebServerRepositoryImpl(private val service: ArduinoData): WebServerRepository {

    override suspend fun saveUserId(userId: String) =
        coroutineScope {
            try {
                val user = User(userId = userId)

                Result.Success(service.saveUserId(user))
            } catch (ex: Exception) {
                Result.Error(ex.toErrorEntity())
            }
    }

    override suspend fun checkConnection() =
        coroutineScope {
            try {
                service.checkStatusConnection().let(::toDomain)
            } catch(ex: Exception) {
                StatusConnection(
                    isLogged = false,
                    status = StatusConnectionWifi.DISCONNECTED
                )
            }
        }

    private fun toDomain(response: StatusConnectionResponse): StatusConnection {
        val status = StatusConnectionWifi.from(response.status)

        return StatusConnection(
            isLogged = response.isLogged.toBoolean(),
            status = if (!response.isLogged.toBoolean()) StatusConnectionWifi.DISCONNECTED else status
        )
    }
}