package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["value"], unique = true)])
data class ConditionerAirTemperature(
    @PrimaryKey(autoGenerate = true) val idTemperature: Int = 0,
    @ColumnInfo(name = "value") val value: Int
) {
    override fun toString(): String {
        return value.toString()
    }
}