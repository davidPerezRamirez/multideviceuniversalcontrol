package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirTemperature

object ConditionerAirTemperatureConfig {

    fun create(): ArrayList<ConditionerAirTemperature> {
        return arrayListOf(
            ConditionerAirTemperature(value = 17),
            ConditionerAirTemperature(value = 18),
            ConditionerAirTemperature(value = 19),
            ConditionerAirTemperature(value = 20),
            ConditionerAirTemperature(value = 21),
            ConditionerAirTemperature(value = 22),
            ConditionerAirTemperature(value = 23),
            ConditionerAirTemperature(value = 24),
            ConditionerAirTemperature(value = 25),
            ConditionerAirTemperature(value = 26),
            ConditionerAirTemperature(value = 27),
            ConditionerAirTemperature(value = 28),
            ConditionerAirTemperature(value = 29),
            ConditionerAirTemperature(value = 30)
        )
    }
}