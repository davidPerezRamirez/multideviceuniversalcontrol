package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirMode
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.COOL_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HEAT_MODE

object ConditionerAirModeConfig {

    fun create(): ArrayList<ConditionerAirMode> {
        return arrayListOf(
            ConditionerAirMode(name = COOL_MODE),
            ConditionerAirMode(name = HEAT_MODE),
            ConditionerAirMode(name = "OFF")
        )}
}