package com.example.multideviceuniversalcontrol.infraestructure.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.multideviceuniversalcontrol.domain.DataBaseBackup
import com.example.multideviceuniversalcontrol.utils.Converters
import com.example.multideviceuniversalcontrol.domain.room.contracts.*
import com.example.multideviceuniversalcontrol.infraestructure.room.config.*
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.*

@Database(
    entities = [
        DeviceType::class,
        DeviceButton::class,
        Device::class,
        ButtonTypeDeviceRelation::class,
        ConditionerAirButtonRelation::class,
        ConditionerAirMode::class,
        ConditionerAirTemperature::class,
        ConditionerAirSpeed::class,
        ButtonByDevice::class
    ],
    views = [
        ButtonsControl::class,
        ConditionerAirButtonControl::class,
        ButtonByDeviceView::class,
        ConditionerAirOptionConfiguration::class
    ],
    version = 2
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun deviceTypeRepository(): DeviceTypeRepository
    abstract fun deviceButtonRepository(): DeviceButtonDao
    abstract fun deviceRepository(): DeviceDao
    abstract fun buttonTypeDeviceRelationRepository(): ButtonTypeDeviceRelationDao
    abstract fun buttonsControlRepository(): ButtonsControlDao
    abstract fun buttonByDeviceRepository(): ButtonByDeviceDao
    abstract fun buttonByDeviceViewRepository(): ButtonByDeviceViewDao
    abstract fun conditionerAirButtonsControl(): ConditionerAirButtonsControlDao
    abstract fun conditionerAirModeRepository(): ConditionerAirModeDao
    abstract fun conditionerAirTemperatureRepository(): ConditionerAirTemperatureDao
    abstract fun conditionerAirSpeedRepository(): ConditionerAirSpeedDao
    abstract fun conditionerAirButtonRelationRepository(): ConditionerAirButtonRelationDao
    abstract fun conditionerAirOptionConfigurationRepository(): ConditionerAirOptionConfigurationDao

    companion object {
        const val DATA_BASE_NAME = "untref-tesis"
        var INSTANCE: AppDatabase? = null

        fun createAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = createAppDatabase(context)

                    configure()
                    //loadDeviceData()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }

        private fun createAppDatabase(appContext: Context): AppDatabase {
            return Room.databaseBuilder(
                appContext,
                AppDatabase::class.java,
                DATA_BASE_NAME
            )
                .allowMainThreadQueries()
                .build()
        }

        private fun configure() {
            configureDeviceData()
            configureConditioningAir()
        }

        private fun configureDeviceData() {
            val deviceTypes = DeviceTypesConfig.create()
            val deviceButtons = DeviceButtonConfig.create()
            val buttonsByDevice = ButtonByDeviceConfig.create()

            INSTANCE?.deviceTypeRepository()?.insertAll(deviceTypes = deviceTypes)
            INSTANCE?.deviceButtonRepository()?.insertAll(deviceButtons = deviceButtons)
            INSTANCE?.buttonByDeviceRepository()?.insertAll(buttonsByDevice = buttonsByDevice)
        }

        private fun configureConditioningAir() {
            val modes = ConditionerAirModeConfig.create()
            val temperatures = ConditionerAirTemperatureConfig.create()
            val speeds = ConditionerAirSpeedConfig.create()

            INSTANCE?.conditionerAirModeRepository()?.insertAll(modes)
            INSTANCE?.conditionerAirTemperatureRepository()?.insertAll(temperatures)
            INSTANCE?.conditionerAirSpeedRepository()?.insertAll(speeds)
        }

        private fun loadDeviceData() {
            val hasNotDevices = INSTANCE?.deviceRepository()?.getAll().isNullOrEmpty()

            if (hasNotDevices) {
                val devices = DeviceConfig.create()
                val buttonTypeDeviceRelations = ButtonTypeDeviceRelationConfig.create()
                val conditionerAirButtonRelation = ConditionerAirButtonRelationConfig.create()

                INSTANCE?.deviceRepository()?.insertAll(devices = devices)
                INSTANCE?.buttonTypeDeviceRelationRepository()
                    ?.insertAll(buttonTypeDeviceRelations = buttonTypeDeviceRelations)
                INSTANCE?.conditionerAirButtonRelationRepository()
                    ?.insertAll(conditionerAirButtonRelation)
            }
        }

        fun loadDeviceData(backup: DataBaseBackup) {
            //TODO chequear que haya datos en backup
            INSTANCE?.deviceRepository()?.insertAll(devices = backup.devices)
            INSTANCE?.buttonTypeDeviceRelationRepository()
                ?.insertAll(buttonTypeDeviceRelations = backup.deviceData)
            INSTANCE?.conditionerAirButtonRelationRepository()
                ?.insertAll(backup.conditionedAirData)

        }
    }
}