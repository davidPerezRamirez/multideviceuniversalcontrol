package com.example.multideviceuniversalcontrol.infraestructure.repository

import android.content.SharedPreferences

class PreferencesRepository (private val preferences: SharedPreferences) {

    fun save(preferenceId: String, value: String) {
        preferences
            .edit()
            .putString(preferenceId, value)
            .apply()
    }

    fun save(preferenceId: String, value: Int) {
        preferences
            .edit()
            .putInt(preferenceId, value)
            .apply()
    }

    fun getString(id: String): String {
        return preferences.getString(id, "")!!
    }

    fun getInt(id: String): Int {
        return preferences.getInt(id, -1)
    }
}