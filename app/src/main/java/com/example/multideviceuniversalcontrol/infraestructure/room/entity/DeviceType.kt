package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["name"], unique = true)])
data class DeviceType(

    @PrimaryKey(autoGenerate = true) var idDeviceType: Int = 0,
    @ColumnInfo(name = "name") var name: String
) {
    override fun toString(): String {
        return name
    }
}