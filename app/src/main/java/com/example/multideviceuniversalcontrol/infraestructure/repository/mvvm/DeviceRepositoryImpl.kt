package com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm

import com.example.multideviceuniversalcontrol.domain.SendNECSignalResponse
import com.example.multideviceuniversalcontrol.domain.SendSignalStatus
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.DeviceRepository
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.toErrorEntity
import kotlinx.coroutines.coroutineScope

class DeviceRepositoryImpl(
    private val service: ArduinoData,
    private val firebaseService: FirebaseService
): DeviceRepository {

    override suspend fun sendSignal(idDevice: Int, nameButton: String) =
        coroutineScope {
        try {
            val deviceButton  = AppDatabase.INSTANCE!!.buttonsControlRepository()
                .findButtonFromDevice(idDevice, nameButton)

            val response = if (deviceButton != null) {
                service.sendNEC(deviceButton.code).let (::toDomain)
            } else {
                SendNECSignalResponse(
                    status = SendSignalStatus.NOT_FOUND
                )
            }

            Result.Success(response)
        } catch (ex: Exception) {
            Result.Error(ex.toErrorEntity())
        }
    }

    private fun toDomain(response: ResponseArduino): SendNECSignalResponse {
        return SendNECSignalResponse(
            status = SendSignalStatus.from(response.status)
        )
    }

    override suspend fun saveCodeInCloud(onFail: () -> Unit, idDevice: Int, nameButton: String) =
        coroutineScope{

        val deviceButton  = AppDatabase.INSTANCE!!.buttonsControlRepository()
            .findButtonFromDevice(idDevice, nameButton)!!

        firebaseService.saveCode(onFail, deviceButton.code)
    }

}