package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.DatabaseView

@DatabaseView(
    "SELECT bbd.idDeviceType, db.name as nameButton " +
            "FROM ButtonByDevice bbd " +
            "INNER JOIN DeviceButton db ON bbd.idDeviceButton = db.idDeviceButton"
)
data class ButtonByDeviceView(
    val idDeviceType: Int,
    val nameButton: String
) {
    override fun toString(): String {
        return nameButton
    }
}