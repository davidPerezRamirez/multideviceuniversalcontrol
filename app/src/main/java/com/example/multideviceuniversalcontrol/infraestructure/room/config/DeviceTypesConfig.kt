package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants

object DeviceTypesConfig {

    fun create(): ArrayList<DeviceType> {
        return arrayListOf(
            DeviceType(name = DeviceTypeConstants.TELEVISION),
            DeviceType(name = DeviceTypeConstants.AUDIO),
            DeviceType(name = DeviceTypeConstants.AIR_CONDITIONER)
        )
    }

}
