package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.*

@Entity(
    indices = [Index(value = ["idDevice","idMode", "idTemperature", "idSpeed"], unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = Device::class,
            parentColumns = ["idDevice"],
            childColumns = ["idDevice"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ConditionerAirMode::class,
            parentColumns = ["idMode"],
            childColumns = ["idMode"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        ), ForeignKey(
            entity = ConditionerAirTemperature::class,
            parentColumns = ["idTemperature"],
            childColumns = ["idTemperature"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        ), ForeignKey(
            entity = ConditionerAirSpeed::class,
            parentColumns = ["idSpeed"],
            childColumns = ["idSpeed"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class ConditionerAirButtonRelation (
    @PrimaryKey(autoGenerate = true) val idConditionerAirButtonRelation: Int = 0,
    @ColumnInfo(name = "idDevice") val idDevice: Int,
    @ColumnInfo(name = "idMode") val idMode: Int,
    @ColumnInfo(name = "idTemperature") val idTemperature: Int,
    @ColumnInfo(name = "idSpeed") val idSpeed: Int,
    @ColumnInfo(name = "code") val code: List<Int>
)