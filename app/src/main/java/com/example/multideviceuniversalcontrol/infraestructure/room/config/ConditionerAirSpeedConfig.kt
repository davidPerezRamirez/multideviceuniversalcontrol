package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirSpeed
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HIGH_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.LOW_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.NORMAL_SPEED

object ConditionerAirSpeedConfig {

    fun create(): ArrayList<ConditionerAirSpeed> {
        return arrayListOf(
            ConditionerAirSpeed(description = "Sin viento"),
            ConditionerAirSpeed(description = LOW_SPEED),
            ConditionerAirSpeed(description = NORMAL_SPEED),
            ConditionerAirSpeed(description = HIGH_SPEED)
        )
    }
}