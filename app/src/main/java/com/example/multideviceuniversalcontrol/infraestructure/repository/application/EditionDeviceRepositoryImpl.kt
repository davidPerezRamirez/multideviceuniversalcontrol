package com.example.multideviceuniversalcontrol.infraestructure.repository.application

import com.example.multideviceuniversalcontrol.domain.SaveCodeResult
import com.example.multideviceuniversalcontrol.domain.SignalInfo
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.EditionDeviceRepository
import com.example.multideviceuniversalcontrol.infraestructure.repository.StatusMode
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirButtonRelation
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonTypeDeviceRelation
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.toErrorEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class EditionDeviceRepositoryImpl(private val service: ArduinoData): EditionDeviceRepository {

    override suspend fun changeEditionMode(statusMode: StatusMode) =
        coroutineScope  {
         try {
            Result.Success(service.changeEditionMode(statusMode.toString()))
        } catch (ex: Exception) {
            Result.Error(ex.toErrorEntity())
        }
    }

    override suspend fun saveSignalCode(signalInfo: SignalInfo) =
        coroutineScope {
            try {
                val decodedCode = service.getDecodedMode()
                val device = AppDatabase.INSTANCE!!.deviceRepository().findById(signalInfo.idDevice)

                if (isConditionerAir(device.idDeviceType)) {
                    val speedName = signalInfo.extraData!![ButtonContants.AC_SPEED_KEY].toString()
                    val modeName = signalInfo.extraData[ButtonContants.AC_MODE_KEY].toString()
                    val temperature = signalInfo.extraData[ButtonContants.AC_TEMPERATURE_KEY] as Int
                    val idSpeed = AppDatabase.INSTANCE!!.conditionerAirSpeedRepository().findByName(speedName).idSpeed
                    val idMode = AppDatabase.INSTANCE!!.conditionerAirModeRepository().findByName(modeName).idMode
                    val idTemperature = AppDatabase.INSTANCE!!.conditionerAirTemperatureRepository().findByName(temperature).idTemperature

                    AppDatabase.INSTANCE!!.conditionerAirButtonRelationRepository()
                        .insert(
                            ConditionerAirButtonRelation(
                                idDevice = signalInfo.idDevice,
                                idMode = idMode,
                                idTemperature = idTemperature,
                                idSpeed = idSpeed,
                                code = decodedCode.rawCode
                            )
                        )
                } else {
                    val nameButton = signalInfo.extraData!![ButtonContants.NAME_BUTTON_KEY].toString()
                    val idDeviceButton = AppDatabase.INSTANCE!!.deviceButtonRepository()
                        .findByName(nameButton).idDeviceButton

                    AppDatabase.INSTANCE!!.buttonTypeDeviceRelationRepository().insert(
                        ButtonTypeDeviceRelation(
                            idDevice = signalInfo.idDevice,
                            idDeviceButton = idDeviceButton,
                            code = decodedCode.hexaCode
                        )
                    )
                }
                Result.Success(SaveCodeResult(device, decodedCode))
            } catch (ex: Exception) {
                Result.Error(ex.toErrorEntity())
            }
        }

    override fun isConditionerAir(idDeviceType: Int): Boolean {
        val deviceType = AppDatabase.INSTANCE!!.deviceTypeRepository().findById(idDeviceType)

        return deviceType.name == DeviceTypeConstants.AIR_CONDITIONER
    }
}