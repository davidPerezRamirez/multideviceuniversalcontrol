package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.Device

object DeviceConfig {

    fun create(): ArrayList<Device> {
        return arrayListOf(
            Device(idDeviceType = 1, name = "Tv Comedor"),
            Device(idDeviceType = 2, name = "Eq. pieza"),
            Device(idDeviceType = 3, name = "Aire Comedor")
        )
    }
}