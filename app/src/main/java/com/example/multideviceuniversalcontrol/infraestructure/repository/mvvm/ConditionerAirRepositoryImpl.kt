package com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm

import com.example.multideviceuniversalcontrol.domain.SendRawSignalResponse
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.SendSignalStatus
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.ConditionerAirRepository
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.toErrorEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class ConditionerAirRepositoryImpl(
    private val service: ArduinoData,
    private val firebaseService: FirebaseService
): ConditionerAirRepository {

    override suspend fun sendConditionerAirSignal(idDevice: Int, mode: String, temp: Int, speed: String) =
        withContext(Dispatchers.IO) {
            try {
                val deviceButton = AppDatabase.INSTANCE!!.conditionerAirButtonsControl().findButton(idDevice, mode, temp, speed)
                val response = if (deviceButton != null) {
                    val code = deviceButton.code.substring(1, deviceButton.code.length - 1)

                    service.sendRaw(code).let(::toDomain)
                } else {
                    SendRawSignalResponse(
                        status = SendSignalStatus.NOT_FOUND
                    )
                }

                Result.Success(response)
            } catch (ex: Exception) {
                Result.Error(ex.toErrorEntity())
            }
        }

    override suspend fun sendConditionerAirOffSignal(idDevice: Int) =
        coroutineScope {
            try {
                val deviceButton = AppDatabase.INSTANCE!!.conditionerAirButtonsControl()
                    .findOffButton(idDevice)

                val response = if (deviceButton != null) {
                    val code = deviceButton.code.substring(1, deviceButton.code.length - 1)

                    service.sendRaw(code).let {
                        toDomain(it, isOffButton = true)
                    }
                } else {
                    SendRawSignalResponse(
                        status = SendSignalStatus.NOT_FOUND,
                        isOffButton = true
                    )
                }

                Result.Success(response)
            } catch (ex: Exception) {
                Result.Error(ex.toErrorEntity())
            }
        }

    private fun toDomain(response: ResponseArduino, isOffButton: Boolean = false): SendRawSignalResponse {
        return SendRawSignalResponse(
            status = SendSignalStatus.from(response.status),
            isOffButton = isOffButton
        )
    }

    override suspend fun saveCodeInCloud(
        onFail: () -> Unit,
        idDevice: Int,
        mode: String?,
        temp: Int?,
        speed: String?
    ) = coroutineScope {
        val deviceButton = if (mode == null && temp == null && speed == null) {
            AppDatabase.INSTANCE!!.conditionerAirButtonsControl().findOffButton(idDevice)
        } else {
            AppDatabase.INSTANCE!!.conditionerAirButtonsControl().findButton(idDevice, mode!!, temp!!, speed!!)
        }
        val code = deviceButton!!.code.substring(1, deviceButton.code.length - 1)

        firebaseService.saveCode(onFail, code)
    }

}