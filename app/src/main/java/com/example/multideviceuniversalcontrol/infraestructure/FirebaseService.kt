package com.example.multideviceuniversalcontrol.infraestructure

import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class FirebaseService(private val context: Context) {

    private var signInClient: GoogleSignInClient

    init {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        signInClient = GoogleSignIn.getClient(context, gso)
    }

    fun getUserId(): String? {
        return GoogleSignIn.getLastSignedInAccount(context)?.id
    }

    fun isUserLogger(): Boolean {
        return !getUserId().isNullOrEmpty()
    }

    fun getSignInClient(): GoogleSignInClient {
        return signInClient
    }

    fun handleSignInResult(data: Intent?, handleResult: (String) -> Unit) {
        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)

        try {
            val account = task.getResult(ApiException::class.java)

            account?.id?.let {
                handleResult(it)
            }
            //TODO mostrar popup rojo de que algo salio mal al loguearse con ese mail
        } catch (e: ApiException) {
            Log.w("Splash Activity", "signInResult:failed code=" + e.statusCode)
            //TODO mostrar popup rojo de que algo salio mal al loguearse con ese mail
        }
    }

    fun saveCode(onFail:() -> Unit, code: String) {
        //TODO tiene un delay de 10 segundos
        getUserId()?.let {
            val database = Firebase.database
            val reference = database.getReference(it)

            reference
                .setValue(code)
                .addOnFailureListener { onFail() }
        }
    }
}