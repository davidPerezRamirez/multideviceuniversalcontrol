package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.DatabaseView

@DatabaseView(
    "SELECT aabr.idDevice, aam.name as mode, aas.description as speed, aat.value as temperature " +
            "FROM ConditionerAirButtonRelation aabr " +
            "INNER JOIN ConditionerAirMode aam ON aabr.idMode = aam.idMode " +
            "INNER JOIN ConditionerAirSpeed aas ON aabr.idSpeed = aas.idSpeed " +
            "INNER JOIN ConditionerAirTemperature aat ON aabr.idTemperature = aat.idTemperature "
)
data class ConditionerAirOptionConfiguration(
    val idDevice: String,
    val mode: String,
    val speed: String,
    val temperature: Int
)