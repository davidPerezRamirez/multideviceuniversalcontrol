package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.DatabaseView

@DatabaseView(
    "SELECT dt.idDevice, db.name as nameButton, btdr.code " +
            "FROM ButtonTypeDeviceRelation btdr " +
            "INNER JOIN DeviceButton db ON btdr.idDeviceButton = db.idDeviceButton " +
            "INNER JOIN Device dt ON btdr.idDevice = dt.idDevice"
)
data class ButtonsControl(
    val idDevice: Int,
    val nameButton: String,
    val code: String
)