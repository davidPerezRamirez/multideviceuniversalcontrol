package com.example.multideviceuniversalcontrol.infraestructure.network

import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitHttp {

    companion object {

        var INSTANCE: ArduinoData? = null

        fun getRetrofitConnection(): ArduinoData {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = connect()
                }
            }
            return INSTANCE!!
        }

        private fun connect(): ArduinoData {
            val okHttpClient = OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .build()

            val gson = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()

            return Retrofit.Builder()
                .baseUrl("http://192.168.0.10/")
                .addConverterFactory(
                    GsonConverterFactory
                        .create(gson)
                )
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ArduinoData::class.java)
        }
    }
}