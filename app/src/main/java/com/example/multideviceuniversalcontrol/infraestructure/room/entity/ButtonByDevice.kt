package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.*

@Entity(
    indices = [Index(value = ["idDeviceType", "idDeviceButton"], unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = DeviceType::class,
            parentColumns = ["idDeviceType"],
            childColumns = ["idDeviceType"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = DeviceButton::class,
            parentColumns = ["idDeviceButton"],
            childColumns = ["idDeviceButton"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class ButtonByDevice(

    @PrimaryKey(autoGenerate = true) val idButtonByDevice: Int = 0,
    @ColumnInfo(name = "idDeviceType") val idDeviceType: Int,
    @ColumnInfo(name = "idDeviceButton") val idDeviceButton: Int,
)
