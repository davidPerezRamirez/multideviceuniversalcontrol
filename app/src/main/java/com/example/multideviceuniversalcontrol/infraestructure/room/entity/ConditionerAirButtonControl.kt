package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.DatabaseView

@DatabaseView(
    "SELECT aabr.idDevice, aam.name as mode, aat.value as temperature, aas.description as speed, aabr.code " +
            "FROM ConditionerAirButtonRelation aabr " +
            "INNER JOIN ConditionerAirMode aam ON aabr.idMode = aam.idMode " +
            "INNER JOIN conditionerAirTemperature aat ON aabr.idTemperature = aat.idTemperature " +
            "INNER JOIN ConditionerAirSpeed aas ON aabr.idSpeed = aas.idSpeed"
)
data class ConditionerAirButtonControl (
    val idDevice: Int,
    val mode: String,
    val temperature: Int,
    val speed: String,
    val code: String
)