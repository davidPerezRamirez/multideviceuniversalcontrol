package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["description"], unique = true)])
data class ConditionerAirSpeed (
    @PrimaryKey(autoGenerate = true) val idSpeed: Int = 0,
    @ColumnInfo(name = "description") val description: String
) {
    override fun toString(): String {
        return description
    }
}