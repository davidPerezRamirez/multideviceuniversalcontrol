package com.example.multideviceuniversalcontrol.infraestructure.repository

enum class StatusMode {
    ACTIVE,
    DESACTIVE
}