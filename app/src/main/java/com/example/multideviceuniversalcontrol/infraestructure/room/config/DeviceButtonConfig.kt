package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceButton
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants

object DeviceButtonConfig {

    fun create(): ArrayList<DeviceButton> {
        return arrayListOf(
            DeviceButton(name = ButtonContants.ON_OFF),
            DeviceButton(name = ButtonContants.VOLUMEN_UP),
            DeviceButton(name = ButtonContants.VOLUMEN_DOWN),
            DeviceButton(name = ButtonContants.CHANNEL_UP),
            DeviceButton(name = ButtonContants.CHANNEL_DOWN),
            DeviceButton(name = ButtonContants.MUTE),
            DeviceButton(name = ButtonContants.BACK),
            DeviceButton(name = ButtonContants.ZERO),
            DeviceButton(name = ButtonContants.ONE),
            DeviceButton(name = ButtonContants.TWO),
            DeviceButton(name = ButtonContants.THREE),
            DeviceButton(name = ButtonContants.FOUR),
            DeviceButton(name = ButtonContants.FIVE),
            DeviceButton(name = ButtonContants.SIX),
            DeviceButton(name = ButtonContants.SEVEN),
            DeviceButton(name = ButtonContants.EIGHT),
            DeviceButton(name = ButtonContants.NINE),
            DeviceButton(name = ButtonContants.PLAY),
            DeviceButton(name = ButtonContants.PAUSE),
            DeviceButton(name = ButtonContants.UP),
            DeviceButton(name = ButtonContants.DOWN),
            DeviceButton(name = ButtonContants.OK),
            DeviceButton(name = ButtonContants.RIGHT),
            DeviceButton(name = ButtonContants.LEFT),
            DeviceButton(name = ButtonContants.HOME),

            /*  System audio button  */
            DeviceButton(name = ButtonContants.BASS_UP),
            DeviceButton(name = ButtonContants.BASS_DOWN),
            DeviceButton(name = ButtonContants.TREBLE_UP),
            DeviceButton(name = ButtonContants.TREBLE_DOWN)
        )
    }

}