package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import androidx.room.*

@Entity(
    indices = [Index(value = ["name", "idDeviceType"], unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = DeviceType::class,
            parentColumns = ["idDeviceType"],
            childColumns = ["idDeviceType"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class Device(

    @PrimaryKey(autoGenerate = true) val idDevice: Int = 0,
    @ColumnInfo(name = "idDeviceType") val idDeviceType: Int,
    @ColumnInfo(name = "name") val name: String
) {

    override fun toString(): String {
        return name
    }
}
