package com.example.multideviceuniversalcontrol.infraestructure.room.config

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonByDevice

object ButtonByDeviceConfig {

    fun create(): ArrayList<ButtonByDevice> {
        return arrayListOf(
            //Television
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 1),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 2),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 3),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 4),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 5),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 6),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 7),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 8),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 9),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 10),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 11),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 12),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 13),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 14),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 15),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 16),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 17),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 18),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 19),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 20),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 21),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 22),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 23),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 24),
            ButtonByDevice(idDeviceType = 1, idDeviceButton = 25),

            //Music Equipment
            ButtonByDevice(idDeviceType = 2, idDeviceButton = 1),
            ButtonByDevice(idDeviceType = 2, idDeviceButton = 6),
            ButtonByDevice(idDeviceType = 2, idDeviceButton = 26),
            ButtonByDevice(idDeviceType = 2, idDeviceButton = 27),
            ButtonByDevice(idDeviceType = 2, idDeviceButton = 28),
            ButtonByDevice(idDeviceType = 2, idDeviceButton = 29),
        )
    }
}