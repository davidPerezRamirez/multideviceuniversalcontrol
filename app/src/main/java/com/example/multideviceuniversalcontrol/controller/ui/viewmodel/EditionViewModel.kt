package com.example.multideviceuniversalcontrol.controller.ui.viewmodel

import android.database.sqlite.SQLiteConstraintException
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.multideviceuniversalcontrol.controller.factory.FileRepository
import com.example.multideviceuniversalcontrol.domain.InsertStatus
import com.example.multideviceuniversalcontrol.domain.SaveCodeResult
import com.example.multideviceuniversalcontrol.domain.SignalInfo
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.EditionDeviceRepository
import com.example.multideviceuniversalcontrol.domain.arduino.model.ChangeEditionResponse
import com.example.multideviceuniversalcontrol.infraestructure.repository.StatusMode
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.*
import com.example.multideviceuniversalcontrol.utils.Result
import kotlinx.coroutines.launch

class EditionViewModel(
    private val repository: EditionDeviceRepository,
    private val fileRepository: FileRepository
): ViewModel() {

    private var _devices = MutableLiveData<List<Device>>()
    val devices : LiveData<List<Device>> get() = _devices
    private var _buttonByDeviceView = MutableLiveData<List<ButtonByDeviceView>>()
    val buttonByDeviceView : LiveData<List<ButtonByDeviceView>> get() = _buttonByDeviceView
    private var _temperatures = MutableLiveData<List<ConditionerAirTemperature>>()
    val temperatures : LiveData<List<ConditionerAirTemperature>> get() = _temperatures
    private var _modes = MutableLiveData<List<ConditionerAirMode>>()
    val modes : LiveData<List<ConditionerAirMode>> get() = _modes
    private var _speeds = MutableLiveData<List<ConditionerAirSpeed>>()
    val speeds : LiveData<List<ConditionerAirSpeed>> get() = _speeds

    private var _resultChangeEditionMode = MutableLiveData<Result<ChangeEditionResponse>>()
    val resultChangeEditionMode: LiveData<Result<ChangeEditionResponse>> get() = _resultChangeEditionMode

    private var _resultSaveCode = MutableLiveData<Result<SaveCodeResult>>()
    val resultSaveCodeCode: LiveData<Result<SaveCodeResult>> get() = _resultSaveCode

    private var _abmDevice = MutableLiveData<InsertStatus>()
    val abmDevice: LiveData<InsertStatus> get() = _abmDevice

    fun changeEditionMode(statusMode: StatusMode) {
        viewModelScope.launch {
            _resultChangeEditionMode.postValue(repository.changeEditionMode(statusMode))
        }
    }

    fun getDevices() {
        _devices.postValue(
            devices()
        )
    }

    fun devices(): List<Device> {
        return AppDatabase.INSTANCE!!.deviceRepository().getAll()
    }

    fun getButtonsByDevice(idDeviceType: Int) {
        _buttonByDeviceView.postValue(
            AppDatabase.INSTANCE!!.buttonByDeviceViewRepository().findByIdDeviceType(idDeviceType)
        )
    }

    fun getTemperatures() {
        _temperatures.postValue(
            AppDatabase.INSTANCE!!.conditionerAirTemperatureRepository().getAll()
        )
    }

    fun getModes() {
        _modes.postValue(
            AppDatabase.INSTANCE!!.conditionerAirModeRepository().getAll()
        )
    }

    fun getSpeeds() {
        _speeds.postValue(
            AppDatabase.INSTANCE!!.conditionerAirSpeedRepository().getAll()
        )
    }

    fun saveSignalCode(signal: SignalInfo) {
        viewModelScope.launch {
            _resultSaveCode.postValue(repository.saveSignalCode(signal))
        }
    }

    fun isconditionerAir(device: Device): Boolean {
        return repository.isConditionerAir(device.idDeviceType)
    }

    fun createDataBaseBackup() {
        fileRepository.createDatabaseBackup()
    }

    fun getDeviceTypes(): List<DeviceType> {
        return AppDatabase.INSTANCE!!.deviceTypeRepository().getAll()
    }

    fun addDevice(idDeviceType: Int, deviceName: String) {
        val insertStatus = try {
            val newDevice = Device(
                idDeviceType = idDeviceType,
                name = deviceName
            )
            val idInsert = AppDatabase.INSTANCE!!.deviceRepository().insert(newDevice)
            if (idInsert > 0 ) {
                InsertStatus.INSERTED
            } else {
                InsertStatus.ABORTED
            }
        } catch (ex: SQLiteConstraintException) {
            InsertStatus.ABORTED
        }

        _abmDevice.postValue(insertStatus)
    }

    fun editDevice(device: Device) {
        val insertStatus = try {
            val countRowsDeleted = AppDatabase.INSTANCE!!.deviceRepository().updateNameDevice(device)
            if (countRowsDeleted > 0 ) {
                InsertStatus.UPDATED
            } else {
                InsertStatus.ABORTED
            }
        } catch (ex: SQLiteConstraintException) {
            InsertStatus.ABORTED
        }
        _abmDevice.postValue(insertStatus)
    }

    fun deleteDevice(device: Device) {
        val insertStatus = try {
            val countRowsUpdated = AppDatabase.INSTANCE!!.deviceRepository().delete(device)
            if (countRowsUpdated > 0 ) {
                InsertStatus.DELETE
            } else {
                InsertStatus.ABORTED
            }
        } catch (ex: SQLiteConstraintException) {
            InsertStatus.ABORTED
        }
        _abmDevice.postValue(insertStatus)
    }

    fun buttonsConfigured(device: Device): List<Any> {

        return if (isconditionerAir(device)) {
            AppDatabase.INSTANCE!!.conditionerAirOptionConfigurationRepository().getOptionConfiguration(device.idDevice)
        } else {
            AppDatabase.INSTANCE!!.buttonsControlRepository().getButtonsDevice(device.idDevice)
                .map { it.nameButton }
        }
    }
}