package com.example.multideviceuniversalcontrol.controller.ui.fragment

import android.content.Intent
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentActivity
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.factory.PopUpFactory
import com.example.multideviceuniversalcontrol.controller.network.NetworkChecker
import com.example.multideviceuniversalcontrol.controller.network.NetworkType
import com.example.multideviceuniversalcontrol.controller.ui.EditionActivity
import com.example.multideviceuniversalcontrol.domain.SignalInfo
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm.DeviceRepositoryImpl
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.lazyFindViewById
import com.example.multideviceuniversalcontrol.controller.ui.viewmodel.DeviceViewModel
import com.example.multideviceuniversalcontrol.domain.SendNECSignalResponse
import com.example.multideviceuniversalcontrol.domain.SendSignalStatus
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository

abstract class BaseDeviceFragment(
    service: ArduinoData,
    private val networkChecker: NetworkChecker
): BaseFragment(service) {

    private val viewModel: DeviceViewModel by lazy {
        val preferencesRepository =
            PreferencesRepository(requireActivity().getPreferences(FragmentActivity.MODE_PRIVATE))
        val firebaseService = FirebaseService(this.requireActivity())
        val repository = DeviceRepositoryImpl(service, firebaseService)

        DeviceViewModel(repository, preferencesRepository)
    }

    private fun sendSignal(signal: SignalInfo) {
        val nameButton = signal.extraData!![ButtonContants.NAME_BUTTON_KEY].toString()

        when (networkChecker.getNetworkType()) {
            NetworkType.LOCAL -> viewModel.sendSignal(signal.idDevice, nameButton)
            NetworkType.EXTERNAL -> {
                viewModel.saveCodeInCloud(
                    { PopUpFactory.showError(container) },
                    signal.idDevice,
                    nameButton
                )
            }
            NetworkType.UNKNOWN -> PopUpFactory.showError(container)
        }
    }

    fun <T : View> initializeButton(container: View, res: Int, nameButton: String, updateMonitorValue: (() -> Unit)? = null) {
        val button by container.lazyFindViewById<T>(res)
        val animation = AnimationUtils.loadAnimation(button.context, R.anim.scale_animation)

        button.setOnClickListener {
            val deviceSelected = viewModel.getIdDeviceSelected(getDeviceTypeName())

            val signalInfo = SignalInfo(
                idDevice = deviceSelected,
                extraData = mapOf(
                    ButtonContants.NAME_BUTTON_KEY to nameButton
                )
            )
            button.startAnimation(animation)
            updateMonitorValue?.let { it() }
            sendSignal(signalInfo)
        }
    }

    fun initializeObservers() {
        viewModel.resultSendSignal.observe(viewLifecycleOwner, ::observerResultSendSignal)
    }

    private fun observerResultSendSignal(response: Result<SendNECSignalResponse>) {
        when (response) {
            is Result.Error -> PopUpFactory.showError(container)
            is Result.Success -> {
                when(response.data.status) {
                    SendSignalStatus.SUCCESS -> Log.i(getDeviceTypeName(), response.data.status.toString())
                    SendSignalStatus.NOT_FOUND -> PopUpFactory.goToEditButton(container, ::goToEditionView)
                    else -> PopUpFactory.showError(container)
                }
            }
        }
    }

    private fun goToEditionView() {
        val intent = Intent(this.requireActivity(), EditionActivity::class.java)
        startActivity(intent)
    }

}