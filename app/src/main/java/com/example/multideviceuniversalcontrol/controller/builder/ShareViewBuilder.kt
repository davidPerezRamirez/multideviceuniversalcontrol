package com.example.multideviceuniversalcontrol.controller.builder

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.widget.Toast
import androidx.core.content.FileProvider
import com.example.multideviceuniversalcontrol.BuildConfig
import java.io.File

object ShareViewBuilder {

    const val IMPORT_CODE = 111

    //TODO revisar porque no permite adjuntar el archivo en el mail
    fun exportFile(
        fileToShare: File,
        context: Context,
        startContext: (Intent) -> Unit
    ) {
        try {
            val uri = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                Uri.fromFile(fileToShare)
            } else {
                FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", fileToShare)
            }
            val subject = "datos_smart_control.json"
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.type = "text/json"
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri)
            emailIntent.flags = (Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startContext(Intent.createChooser(emailIntent, "Sending email..."))
        }
        catch (t: Throwable) {
            Toast.makeText(context, "Request failed try again: $t", Toast.LENGTH_LONG).show()
        }
    }

    fun importFile(startContextForResult: (Intent, Int) -> Unit) {

        val intent = Intent()
            .setType("text/json")
            .setAction(Intent.ACTION_GET_CONTENT)

        startContextForResult(Intent.createChooser(intent, "Select a file"), IMPORT_CODE)
    }
}