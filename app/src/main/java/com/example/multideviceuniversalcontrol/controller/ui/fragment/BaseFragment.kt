package com.example.multideviceuniversalcontrol.controller.ui.fragment

import android.view.View
import androidx.fragment.app.Fragment
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData

abstract class BaseFragment(val service: ArduinoData) : Fragment() {

    protected lateinit var container: View

    abstract fun initializeButtons()

    abstract fun getDeviceTypeName(): String
}