package com.example.multideviceuniversalcontrol.controller.factory

import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.multideviceuniversalcontrol.R
import com.google.android.material.snackbar.Snackbar

object SnackBarFactory {

    fun createSuccessSnackBar(container: ViewGroup, msg: String) {
        val snackBar = Snackbar.make(container, msg, Snackbar.LENGTH_LONG)

        snackBar.setBackgroundTint(
            ContextCompat.getColor(
                container.context,
                R.color.green_600
            )
        )
        snackBar.show()
    }
    fun createErrorSnackBar(container: ViewGroup, msg: String) {
        val snackBar = Snackbar.make(container, msg, Snackbar.LENGTH_LONG)

        snackBar.setBackgroundTint(
            ContextCompat.getColor(
                container.context,
                R.color.red_400
            )
        )
        snackBar.show()
    }
}