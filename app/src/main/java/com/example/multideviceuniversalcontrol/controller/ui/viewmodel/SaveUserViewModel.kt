package com.example.multideviceuniversalcontrol.controller.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.WebServerRepository
import com.example.multideviceuniversalcontrol.utils.Result
import kotlinx.coroutines.launch

class SaveUserViewModel(
    private val webServerRepository: WebServerRepository
) : ViewModel() {

    private var _resultSaveUser = MutableLiveData<Result<ResponseArduino>>()
    val saveUser: LiveData<Result<ResponseArduino>> get() = _resultSaveUser

    fun saveUserId(userId: String) {
        viewModelScope.launch {
            _resultSaveUser.postValue(webServerRepository.saveUserId(userId))
        }
    }
}