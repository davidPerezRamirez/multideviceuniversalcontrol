package com.example.multideviceuniversalcontrol.controller.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.multideviceuniversalcontrol.domain.SendRawSignalResponse
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.ConditionerAirRepository
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.WebServerRepository
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.utils.Result
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ConditionerAirViewModel(
    private val conditionerAirRepository: ConditionerAirRepository,
    private val preferencesRepository: PreferencesRepository,
    private val webServerRepositoryImpl: WebServerRepository
): ViewModel() {

    private var _resultSendSignal = MutableLiveData<Result<SendRawSignalResponse>>()
    val resultSendSignal: LiveData<Result<SendRawSignalResponse>> get() = _resultSendSignal

    fun sendRawSignal(idDevice: Int, mode: String, temp: Int, speed: String) {
        viewModelScope.launch {
            _resultSendSignal.postValue(conditionerAirRepository.sendConditionerAirSignal(idDevice, mode, temp, speed))
        }
    }

    fun sendRawOff(idDevice: Int) {
        viewModelScope.launch {
            _resultSendSignal.postValue(conditionerAirRepository.sendConditionerAirOffSignal(idDevice))
        }
    }

    fun saveCodeInCloud(onFail: () -> Unit, idDevice: Int, mode: String? = null, temp: Int? = null, speed: String? = null) {
        viewModelScope.launch {
            conditionerAirRepository.saveCodeInCloud(onFail, idDevice, mode, temp, speed)
        }
    }

    fun getIdDeviceSelected(deviceTypeName: String): Int {
        return preferencesRepository.getInt(deviceTypeName)
    }

    fun isSynchronizedToWebServer(): Boolean {
        val statusConnection = runBlocking {
            webServerRepositoryImpl.checkConnection()
        }

        return statusConnection.isLogged
    }
}