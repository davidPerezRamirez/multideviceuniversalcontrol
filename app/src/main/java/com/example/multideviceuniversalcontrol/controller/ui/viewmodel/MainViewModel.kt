package com.example.multideviceuniversalcontrol.controller.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.multideviceuniversalcontrol.domain.DataBaseBackup
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.StatusConnection
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.WebServerRepository
import com.example.multideviceuniversalcontrol.domain.network.contracts.DataBaseRepository
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.Device
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType
import com.example.multideviceuniversalcontrol.utils.Result
import kotlinx.coroutines.*

class MainViewModel(
    private val preferencesRepository: PreferencesRepository,
    private val dataBaseRepository: DataBaseRepository,
    private val webServerRepositoryImpl: WebServerRepository
) {

    private var _checkConnection = MutableLiveData<StatusConnection>()
    val checkConnection: LiveData<StatusConnection> get() = _checkConnection

    private var _synchronize = MutableLiveData<Result<ResponseArduino>>()
    val synchronize: LiveData<Result<ResponseArduino>> get() = _synchronize

    fun checkArduinoConnection() {
        runBlocking {
            _checkConnection.postValue(webServerRepositoryImpl.checkConnection())
        }
    }

    fun synchronize(userId: String) {
        runBlocking {
            _synchronize.postValue(webServerRepositoryImpl.saveUserId(userId))
        }
    }

    fun getDeviceByName(nameDevice: String): Device {
        return AppDatabase.INSTANCE!!.deviceRepository().findByName(nameDevice)
    }

    fun getDevicesByType(nameDevice: String): List<Device> {
        val deviceType = AppDatabase.INSTANCE!!.deviceTypeRepository().findByName(nameDevice)

        return AppDatabase.INSTANCE!!.deviceRepository().getAllDeviceByType(deviceType.idDeviceType)
    }

    fun getDeviceById(idDevice: Int): Device {
        return AppDatabase.INSTANCE!!.deviceRepository().findById(idDevice)
    }

    fun getAllDeviceTypes(): List<DeviceType> {
        return runBlocking {
            dataBaseRepository.getDeviceTypesEnabled()
        }
    }

    fun saveDeviceSelected(device: Device) {
        val deviceType = AppDatabase.INSTANCE!!.deviceTypeRepository().findById(device.idDeviceType)

        preferencesRepository.save(deviceType.name, device.idDevice)
    }

    fun getDeviceSelected(key: String): Int {
        val deviceSelected = preferencesRepository.getInt(key)
        val device = getDeviceById(deviceSelected)

        return  if (device == null) -1 else deviceSelected
    }

    fun importFileDataBase(backupDataBase: DataBaseBackup) {
        AppDatabase.loadDeviceData(backupDataBase)
    }
}