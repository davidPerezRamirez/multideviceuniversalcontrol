package com.example.multideviceuniversalcontrol.controller.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.network.NetworkChecker
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.utils.lazyFindViewById

class AudioSystemDeviceFragment(
    service: ArduinoData,
    networkChecker: NetworkChecker
) : BaseDeviceFragment(service, networkChecker) {

    private lateinit var preferencesRepository: PreferencesRepository

    lateinit var tvBass: TextView
    lateinit var tvVolume: TextView
    lateinit var tvTre: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        this.container = inflater.inflate(R.layout.audio_system_fragment, container, false)

        initializeObservers()
        initializePreference()
        initializeInformationPanel()
        initializeButtons()

        return this.container
    }

    private fun initializePreference() {
        preferencesRepository =
            PreferencesRepository(this.requireActivity().getPreferences(Context.MODE_PRIVATE))
        val bass = preferencesRepository.getString(PreferencesKeys.AUDIO_BASS)
        val tre = preferencesRepository.getString(PreferencesKeys.AUDIO_TRE)
        val volume = preferencesRepository.getString(PreferencesKeys.AUDIO_VOLUME)

        if (bass.isBlank()) {
            preferencesRepository.save(
                PreferencesKeys.AUDIO_BASS,
                DEFAULT_BASS
            )
        }
        if (tre.isBlank()) {
            preferencesRepository.save(PreferencesKeys.AUDIO_TRE, DEFAULT_TRE)
        }
        if (volume.isBlank()) {
            preferencesRepository.save(PreferencesKeys.AUDIO_VOLUME, DEFAULT_VOLUME)
        }
    }

    private fun initializeInformationPanel() {
        tvBass = container.lazyFindViewById<TextView>(R.id.tvBass).value
        tvTre = container.lazyFindViewById<TextView>(R.id.tvTreble).value
        tvVolume = container.lazyFindViewById<TextView>(R.id.tvVolume).value

        tvBass.text = preferencesRepository.getString(PreferencesKeys.AUDIO_BASS)
        tvTre.text = preferencesRepository.getString(PreferencesKeys.AUDIO_TRE)
        tvVolume.text = preferencesRepository.getString(PreferencesKeys.AUDIO_VOLUME)
    }

    private fun getCurrentBass() : String {
        val currentBass = tvBass.text.toString()

        this.preferencesRepository.save(PreferencesKeys.AUDIO_BASS, currentBass)

        return currentBass
    }

    private fun getCurrentTre() : String {
        val currentTre = tvTre.text.toString()

        this.preferencesRepository.save(PreferencesKeys.AUDIO_TRE, currentTre)

        return currentTre
    }

    private fun getCurrentVolume() : String {
        val currentVolume = tvVolume.text.toString()

        this.preferencesRepository.save(PreferencesKeys.AUDIO_VOLUME, currentVolume)

        return currentVolume
    }

    override fun initializeButtons() {
        initializeButton<ImageButton>(container, R.id.btnVolUp, ButtonContants.VOLUMEN_UP, ::incrementVolume)
        initializeButton<ImageButton>(container, R.id.btnVolDown, ButtonContants.VOLUMEN_DOWN, ::decrementVolume)
        initializeButton<ImageButton>(container, R.id.btnPower, ButtonContants.ON_OFF)
        initializeButton<ImageButton>(container, R.id.btnBassDown, ButtonContants.BASS_DOWN, ::decrementBass)
        initializeButton<ImageButton>(container, R.id.btnBassUp, ButtonContants.BASS_UP, ::incrementBass)
        initializeButton<ImageButton>(container, R.id.btnTreDown, ButtonContants.TREBLE_DOWN, ::decrementTre)
        initializeButton<ImageButton>(container, R.id.btnTreUp, ButtonContants.TREBLE_UP, ::incrementTre)
        initializeButton<ImageButton>(container, R.id.btnMute, ButtonContants.MUTE)
    }

    private fun incrementVolume() {
        val currentVolume = getCurrentVolume().toInt()

        if (currentVolume < MAX_VOLUME) {
            tvVolume.text = (currentVolume + 1).toString()
            getCurrentVolume()
        }
    }
    private fun decrementVolume() {
        val currentVolume = getCurrentVolume().toInt()

        if (currentVolume > 0) {
            tvVolume.text = (currentVolume - 1).toString()
            getCurrentVolume()
        }
    }

    private fun incrementBass() {
        val currentBass = getCurrentBass().toInt()

        if (currentBass < MAX_BASS) {
            tvBass.text = (currentBass + 1).toString()
            getCurrentBass()
        }
    }

    private fun decrementBass() {
        val currentBass = getCurrentBass().toInt()

        if (currentBass > MIN_BASS) {
            tvBass.text = (currentBass - 1).toString()
            getCurrentBass()
        }
    }

    private fun incrementTre() {
        val currentTre = getCurrentTre().toInt()

        if (currentTre < MAX_TRE) {
            tvTre.text = (currentTre + 1).toString()
            getCurrentTre()
        }
    }

    private fun decrementTre() {
        val currentTre = getCurrentTre().toInt()

        if (currentTre > MIN_TRE) {
            tvTre.text = (currentTre - 1).toString()
            getCurrentTre()
        }
    }

    override fun getDeviceTypeName(): String {
        return DeviceTypeConstants.AUDIO
    }

    companion object {
        const val DEFAULT_BASS = "0"
        const val DEFAULT_TRE = "0"
        const val DEFAULT_VOLUME = "30"

        private const val MAX_VOLUME = 60
        private const val MAX_BASS = 9
        private const val MIN_BASS = -9
        private const val MAX_TRE = 7
        private const val MIN_TRE = -7
    }
}
