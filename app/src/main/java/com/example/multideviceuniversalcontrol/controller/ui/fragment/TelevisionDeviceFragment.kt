package com.example.multideviceuniversalcontrol.controller.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.network.NetworkChecker
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData

class TelevisionDeviceFragment(
    service: ArduinoData,
    networkChecker: NetworkChecker
) : BaseDeviceFragment(service, networkChecker) {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        this.container = inflater.inflate(R.layout.television_fragment, container, false)

        initializeObservers()
        initializeButtons()

        return this.container
    }

    override fun initializeButtons() {
        initializeButton<ImageButton>(container, R.id.btnVolUp, ButtonContants.VOLUMEN_UP)
        initializeButton<ImageButton>(container, R.id.btnVolDown, ButtonContants.VOLUMEN_DOWN)
        initializeButton<ImageButton>(container, R.id.btnPower, ButtonContants.ON_OFF)
        initializeButton<ImageButton>(container, R.id.btnChDown, ButtonContants.CHANNEL_DOWN)
        initializeButton<ImageButton>(container, R.id.btnChUp, ButtonContants.CHANNEL_UP)
        initializeButton<ImageButton>(container, R.id.btnMute, ButtonContants.MUTE)
        initializeButton<Button>(container, R.id.btnZero, ButtonContants.ZERO)
        initializeButton<Button>(container, R.id.btnOne, ButtonContants.ONE)
        initializeButton<Button>(container, R.id.btnTwo, ButtonContants.TWO)
        initializeButton<Button>(container, R.id.btnThree, ButtonContants.THREE)
        initializeButton<Button>(container, R.id.btnFour, ButtonContants.FOUR)
        initializeButton<Button>(container, R.id.btnFive, ButtonContants.FIVE)
        initializeButton<Button>(container, R.id.btnSix, ButtonContants.SIX)
        initializeButton<Button>(container, R.id.btnSeven, ButtonContants.SEVEN)
        initializeButton<Button>(container, R.id.btnEight, ButtonContants.EIGHT)
        initializeButton<Button>(container, R.id.btnNine, ButtonContants.NINE)
        initializeButton<ImageButton>(container, R.id.btnPlay, ButtonContants.PLAY)
        initializeButton<ImageButton>(container, R.id.btnPause, ButtonContants.PAUSE)
        initializeButton<ImageButton>(container, R.id.btnUp, ButtonContants.UP)
        initializeButton<ImageButton>(container, R.id.btnDown, ButtonContants.DOWN)
        initializeButton<ImageButton>(container, R.id.btnOk, ButtonContants.OK)
        initializeButton<ImageButton>(container, R.id.btnRight, ButtonContants.RIGHT)
        initializeButton<ImageButton>(container, R.id.btnLeft, ButtonContants.LEFT)
        initializeButton<ImageButton>(container, R.id.btnHome, ButtonContants.HOME)
        initializeButton<ImageButton>(container, R.id.btnBack, ButtonContants.BACK)
    }

    override fun getDeviceTypeName(): String {
        return DeviceTypeConstants.TELEVISION
    }

}
