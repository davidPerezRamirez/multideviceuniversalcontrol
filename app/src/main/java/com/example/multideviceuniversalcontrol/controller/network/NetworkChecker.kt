package com.example.multideviceuniversalcontrol.controller.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.example.multideviceuniversalcontrol.domain.StatusConnection
import com.example.multideviceuniversalcontrol.domain.StatusConnectionWifi
import kotlinx.coroutines.runBlocking

class NetworkChecker(
    private val context: Context,
    private val checkServerConnection: suspend () -> StatusConnection
) {

    fun getNetworkType(): NetworkType {
        val networkCapabilities = getNetWorkCompatibilities()


        return when {
            networkCapabilities == null -> NetworkType.UNKNOWN
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> NetworkType.EXTERNAL
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                val statusConnection = runBlocking {
                    checkServerConnection()
                }

                when (statusConnection.status) {
                    StatusConnectionWifi.CONNECTED -> NetworkType.LOCAL
                    StatusConnectionWifi.DISCONNECTED -> NetworkType.EXTERNAL
                }
            }
            else -> NetworkType.UNKNOWN
        }
    }

    private fun getNetWorkCompatibilities(): NetworkCapabilities? {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork

        return connectivityManager.getNetworkCapabilities(network)
    }

}

enum class NetworkType {
    EXTERNAL,
    LOCAL,
    UNKNOWN
}