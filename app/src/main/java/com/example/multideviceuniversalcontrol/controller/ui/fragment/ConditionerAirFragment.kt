package com.example.multideviceuniversalcontrol.controller.ui.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.R.*
import com.example.multideviceuniversalcontrol.controller.factory.PopUpFactory
import com.example.multideviceuniversalcontrol.controller.network.NetworkChecker
import com.example.multideviceuniversalcontrol.controller.network.NetworkType
import com.example.multideviceuniversalcontrol.controller.ui.EditionActivity
import com.example.multideviceuniversalcontrol.controller.ui.viewmodel.ConditionerAirViewModel
import com.example.multideviceuniversalcontrol.domain.SendRawSignalResponse
import com.example.multideviceuniversalcontrol.domain.SendSignalStatus
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys.AIR_COND_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys.AIR_COND_POWER
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys.AIR_COND_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys.AIR_COND_TEMP
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.COOL_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HEAT_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HIGH_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.LOW_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.NORMAL_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.POWER_OFF
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.POWER_ON
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm.ConditionerAirRepositoryImpl
import com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm.WebServerRepositoryImpl
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.lazyFindViewById


class ConditionerAirFragment(
    service: ArduinoData,
    private val networkChecker: NetworkChecker
) : BaseFragment(service) {

    private lateinit var preferencesRepository: PreferencesRepository

    private val viewModel: ConditionerAirViewModel by lazy {
        val firebaseService = FirebaseService(this.requireActivity())
        val webServerRepository = WebServerRepositoryImpl(service)
        val repository = ConditionerAirRepositoryImpl(service, firebaseService)
        val preferencesRepository =
            PreferencesRepository(this.requireActivity().getPreferences(FragmentActivity.MODE_PRIVATE))

        ConditionerAirViewModel(repository, preferencesRepository, webServerRepository)
    }

    private lateinit var powerIndicator: ImageView
    private lateinit var currentTemp: TextView
    private lateinit var currentSpeed: TextView
    private lateinit var currentMode: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        this.container = inflater.inflate(layout.conditioner_air_fragment, container, false)

        viewModel.resultSendSignal.observe(viewLifecycleOwner, ::observerResultSendSignal)
        initializeDefaultValues()
        initializeView()
        initializeButtons()

        currentTemp.text = this.preferencesRepository.getInt(AIR_COND_TEMP).toString()
        currentSpeed.text = this.preferencesRepository.getString(AIR_COND_SPEED)
        currentMode.text = this.preferencesRepository.getString(AIR_COND_MODE)
        powerIndicator.visibility = if (this.preferencesRepository.getString(AIR_COND_POWER).equals(POWER_ON))
            View.VISIBLE else View.GONE

        return this.container
    }

    private fun initializeView() {
        powerIndicator = container.lazyFindViewById<ImageView>(R.id.ivPowerIndicator).value
        currentTemp = container.lazyFindViewById<TextView>(R.id.tvTemp).value
        currentSpeed = container.lazyFindViewById<TextView>(R.id.tvSpeed).value
        currentMode = container.lazyFindViewById<TextView>(R.id.tvMode).value
    }

    override fun initializeButtons() {
        val lowSpeed: () -> Unit = {currentSpeed.text = LOW_SPEED}
        val normalSpeed: () -> Unit = {currentSpeed.text = NORMAL_SPEED}
        val highSpeed: () -> Unit = {currentSpeed.text = HIGH_SPEED}
        val coolMode: () -> Unit = {currentMode.text = COOL_MODE}
        val heatMode: () -> Unit = {currentMode.text = HEAT_MODE}

        initializeButton<ImageButton>(R.id.btnPower, ButtonContants.ON_OFF)
        initializeButton<ImageButton>(R.id.btnTempDown, ButtonContants.TEMP_DOWN, this::decrementTemp)
        initializeButton<ImageButton>(R.id.btnTempUp, ButtonContants.TEMP_UP, this::incrementTemp)
        initializeButton<ImageButton>(R.id.btnLow, ButtonContants.SPEED_LOW, lowSpeed)
        initializeButton<ImageButton>(R.id.btnNormal, ButtonContants.SPEED_NORMAL, normalSpeed)
        initializeButton<ImageButton>(R.id.btnHigh, ButtonContants.SPEED_HIGH, highSpeed)
        initializeButton<ImageButton>(R.id.btnCool, ButtonContants.COOL_MODE, coolMode)
        initializeButton<ImageButton>(R.id.btnHeat, ButtonContants.HEAT_MODE, heatMode)
    }

    private fun observerResultSendSignal(response: Result<SendRawSignalResponse>) {
        when (response) {
            is Result.Error -> PopUpFactory.showError(container)
            is Result.Success -> {
                when(response.data.status) {
                    SendSignalStatus.FAILURE -> {
                        if (viewModel.isSynchronizedToWebServer()) {
                            val deviceSelected = viewModel.getIdDeviceSelected(getDeviceTypeName())

                            sendSignalOutsideLocalNetwork(response.data.isOffButton, deviceSelected)
                        } else {
                            PopUpFactory.showError(container)
                        }
                    }
                    SendSignalStatus.NOT_FOUND -> PopUpFactory.goToEditButton(container, ::goToEditionView)
                    SendSignalStatus.SUCCESS -> Log.i(DeviceTypeConstants.AIR_CONDITIONER, response.data.toString())
                }
            }
        }
    }

    private fun goToEditionView() {
        val intent = Intent(this.requireActivity(), EditionActivity::class.java)
        startActivity(intent)
    }

    private fun getCurrentTemp() : Int {
        val currentTemp = Integer.parseInt(currentTemp.text.toString())

        this.preferencesRepository.save(AIR_COND_TEMP, currentTemp)

        return currentTemp
    }

    private fun getCurrentMode() : String {
        val mode = currentMode.text.toString()

        this.preferencesRepository.save(AIR_COND_MODE, mode)

        return mode
    }

    private fun getCurrentSpeed() : String {
        val speed = currentSpeed.text.toString()

        this.preferencesRepository.save(AIR_COND_SPEED, speed)

        return speed
    }

    private fun incrementTemp() {
        if (getCurrentTemp() < MAX_TEMP) {
            currentTemp.text = (getCurrentTemp() + 1).toString()
        }
    }

    private fun decrementTemp() {
        if (getCurrentTemp() > MIN_TEMP) {
            currentTemp.text = (getCurrentTemp() - 1).toString()
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : View> initializeButton(res: Int, nameButton: String, updateMonitorValue: (() -> Unit)? = null) {
        val button by container.lazyFindViewById<T>(res)
        button.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(button.context, anim.scale_animation)

            updateMonitorValue?.let { it() }
            button.startAnimation(animation)
            sendSignal(nameButton)
        }
    }

    private fun initializeDefaultValues() {
        preferencesRepository =
            PreferencesRepository(this.requireActivity().getPreferences(Context.MODE_PRIVATE))
        val temp = preferencesRepository.getInt(AIR_COND_TEMP)
        val mode = preferencesRepository.getString(AIR_COND_MODE)
        val speed = preferencesRepository.getString(AIR_COND_SPEED)
        val power = preferencesRepository.getString(AIR_COND_POWER)

        if (temp < 0) {
            preferencesRepository.save(AIR_COND_TEMP, DEFAULT_TEMP)
        }
        if (mode.isNullOrBlank()) {
            preferencesRepository.save(AIR_COND_MODE, COOL_MODE)
        }
        if (speed.isNullOrBlank()) {
            preferencesRepository.save(AIR_COND_SPEED, LOW_SPEED)
        }
        if(power.isNullOrBlank()) {
            preferencesRepository.save(AIR_COND_POWER, POWER_OFF)
        }
    }

    private fun sendRawSignal(idDevice: Int) {
        setConditionerAirPower(View.VISIBLE, POWER_ON)

        viewModel.sendRawSignal(idDevice, getCurrentMode(), getCurrentTemp(), getCurrentSpeed())
    }

    private fun setConditionerAirPower(visibility: Int, status: String) {
        powerIndicator.visibility = visibility
        this.preferencesRepository.save(AIR_COND_POWER, status)
    }

    private fun sendRawOff(idDevice: Int) {
        setConditionerAirPower(View.GONE, POWER_OFF)
        viewModel.sendRawOff(idDevice)
    }

    private fun itOn(): Boolean {
        return this.preferencesRepository.getString(AIR_COND_POWER).equals(POWER_ON)
    }

    private fun sendSignal(nameButton: String) {
        val deviceSelected = viewModel.getIdDeviceSelected(getDeviceTypeName())

        when(networkChecker.getNetworkType()) {
            NetworkType.EXTERNAL -> {
                val isOffButton = (nameButton == ButtonContants.ON_OFF && itOn())
                sendSignalOutsideLocalNetwork(isOffButton, deviceSelected)
            }
            NetworkType.LOCAL -> {
                if (nameButton == ButtonContants.ON_OFF && itOn()) {
                    sendRawOff(deviceSelected)
                } else {
                    sendRawSignal(deviceSelected)
                }
            }
            NetworkType.UNKNOWN -> PopUpFactory.showError(container)
        }
    }

    private fun sendSignalOutsideLocalNetwork(isOffButton: Boolean, deviceSelected: Int) {
        if (isOffButton) {
            setConditionerAirPower(View.GONE, POWER_OFF)
            viewModel.saveCodeInCloud({ PopUpFactory.showError(container) }, deviceSelected)
        } else {
            setConditionerAirPower(View.VISIBLE, POWER_ON)
            viewModel.saveCodeInCloud(
                { PopUpFactory.showError(container) },
                deviceSelected,
                getCurrentMode(),
                getCurrentTemp(),
                getCurrentSpeed()
            )
        }
    }

    override fun getDeviceTypeName(): String {
        return DeviceTypeConstants.AIR_CONDITIONER
    }

    companion object {
        const val MAX_TEMP = 30
        const val MIN_TEMP = 17
        const val DEFAULT_TEMP = 24
    }
}
