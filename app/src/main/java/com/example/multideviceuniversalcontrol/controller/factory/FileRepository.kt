package com.example.multideviceuniversalcontrol.controller.factory

import android.content.Context
import com.example.multideviceuniversalcontrol.domain.DataBaseBackup
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import java.io.File
import java.io.InputStream

class FileRepository(context: Context) {

    private val gson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    private val file by lazy {
        createDataBaseFile(context)
    }

    private fun createDataBaseFile(context: Context): File {
        val dir = File(context.getExternalFilesDir(null), DATA_BASE_DIRECTORY_NAME)

        if (!dir.exists()) {
            dir.mkdirs()
        }

        return File (dir, AppDatabase.DATA_BASE_NAME + JSON_EXTENSION)
    }

    fun createDatabaseBackup(): File {
        val fileData = DataBaseBackup(
            devices = AppDatabase.INSTANCE!!.deviceRepository().getAll(),
            deviceData = AppDatabase.INSTANCE!!.buttonTypeDeviceRelationRepository().getAll(),
            conditionedAirData = AppDatabase.INSTANCE!!.conditionerAirButtonRelationRepository().getAll(),
        )

        file.writeText(gson.toJson(fileData).toString())

        return file
    }

    fun readBackupDataBase(inputStream: InputStream): DataBaseBackup {
        file.outputStream().use { inputStream.copyTo(it) }
        val jsonString = file.readText(Charsets.UTF_8)

        return gson.fromJson(jsonString, DataBaseBackup::class.java)
    }

    companion object {
        private const val DATA_BASE_DIRECTORY_NAME = "database"
        private const val JSON_EXTENSION = ".json"
    }
}