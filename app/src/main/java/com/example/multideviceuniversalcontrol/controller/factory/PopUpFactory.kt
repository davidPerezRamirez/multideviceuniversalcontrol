package com.example.multideviceuniversalcontrol.controller.factory

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.isVisible
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.factory.model.ABMType
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.Device
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType
import com.google.android.material.floatingactionbutton.FloatingActionButton

object PopUpFactory {

    fun showError(container: View) {

        val view = View.inflate(container.context, R.layout.pop_up_error_layout, null)
        val popupWindow = PopupWindow(
            view,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        val description = view.findViewById<TextView>(R.id.tx_popup_error_first_step)
        val btn = view.findViewById<Button>(R.id.popup_error_btn_close)

        description.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(getErrorDescription(), Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(getErrorDescription())
        }
        btn.setOnClickListener {
            popupWindow.dismiss()
        }

        popupWindow.isOutsideTouchable = false
        popupWindow.isFocusable = false
        popupWindow.showAtLocation(container, Gravity.CENTER,0,0)
    }

    private fun getErrorDescription(): String {
        return StringBuilder()
            .append("Surgió un error al comunicarse con el decodificador.")
            .append("<br>").append("<br>")
            .append("Se recomienda:")
            .append("<br>").append("<br>")
            .append("<ul>")
            .append("<li>&emsp;Reiniciar el decodificador presionado la tecla RST</li>")
            .append("<li>&emsp;Cerrar la aplicación y volver a abrirla</li>")
            .append("</ul>")
            .toString()
    }

    fun addDevice(
        container: ViewGroup,
        devicesTypes: List<DeviceType>,
        addDevice: (idTypeDevice: Int, deviceName: String) -> Unit
    ) {
        abmDevice(
            R.string.title_add_device_header,
            container,
            devicesTypes,
            ABMType.INSERT
        ) { deviceType: DeviceType, nameDevice: String ->
            addDevice(deviceType.idDeviceType, nameDevice)
        }
    }

    fun editDevice(
        container: ViewGroup,
        devices: List<Device>,
        editDevice: (device: Device) -> Unit
    ) {
        abmDevice(
            R.string.title_edit_device_header,
            container,
            devices,
            ABMType.UPDATE
        ) { device: Device, deviceName: String ->
            editDevice(device.copy(name = deviceName))
        }
    }

    fun deleteDevice(
         container: ViewGroup,
         devices: List<Device>,
         deleteDevice: (device: Device) -> Unit
    ) {
        abmDevice(
            R.string.title_delete_device_header,
            container,
            devices,
            ABMType.DELETE
        ) { device: Device, _ ->
            deleteDevice(device)
        }
    }

    private fun <T> abmDevice(
        title: Int,
        container: ViewGroup,
        items: List<T>,
        abmType: ABMType,
        btnOnClick: (device: T, nameDevice: String) -> Unit
    ) {
        val view = View.inflate(container.context, R.layout.pop_up_abm_device_layout, null)
        val popupWindow = PopupWindow(
            view,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        val imgClose = view.findViewById<ImageView>(R.id.abm_device_image_close)
        val btnOk = view.findViewById<FloatingActionButton>(R.id.abm_device_btn_ok)
        val spinner = view.findViewById<Spinner>(R.id.abm_device_sp)
        val editText = view.findViewById<EditText>(R.id.abm_device_ed_name_device)
        val headerText = view.findViewById<TextView>(R.id.abm_device_tx_header)
        val textDescription = view.findViewById<TextView>(R.id.abm_device_tx_description_device)
        val textNameDevice = view.findViewById<TextView>(R.id.abm_device_tx_name_device)
        val textError = view.findViewById<TextView>(R.id.abm_device_tx_error_name_device)

        val visibility = when(abmType) {
            ABMType.INSERT,
            ABMType.UPDATE -> View.VISIBLE
            ABMType.DELETE -> View.GONE
        }
        editText.visibility = visibility
        textDescription.visibility = visibility
        textNameDevice.visibility = visibility

        headerText.text = view.resources.getText(title)
        initializeSpinner(spinner, container.context, items)
        imgClose.setOnClickListener {
            popupWindow.dismiss()
        }
        btnOk.setOnClickListener {
            val deviceName = editText.text.toString()
            val itemSpinner = spinner.selectedItem as T

            if (editText.isVisible && deviceName.isBlank()) {
                textDescription.visibility = View.GONE
                textError.visibility = View.VISIBLE
            } else {
                textError.visibility = View.GONE
                btnOnClick(itemSpinner, deviceName)
                popupWindow.dismiss()
            }
        }

        popupWindow.isOutsideTouchable = false
        popupWindow.isFocusable = true
        popupWindow.showAtLocation(container, Gravity.CENTER,0,0)
    }

    private fun <T> initializeSpinner(devicesSpinner: Spinner, context: Context, items: List<T>) {
        val adapter = ArrayAdapter(context, R.layout.item_edition_spinner, R.id.item_spinner, items)

        devicesSpinner.adapter= adapter
        devicesSpinner.setSelection(0)
    }

    fun goToEditButton(container: View, goToEdition: () -> Unit) {
        val view = View.inflate(container.context, R.layout.pop_up_go_edit_button, null)
        val popupWindow = PopupWindow(
            view,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        val btn = view.findViewById<Button>(R.id.popup_edit_button_btn_go_to_edition)
        val imgClose = view.findViewById<ImageView>(R.id.img_popup_edit_button_close)

        btn.setOnClickListener {
            goToEdition()
            popupWindow.dismiss()
        }
        imgClose.setOnClickListener {
            popupWindow.dismiss()
        }

        popupWindow.isOutsideTouchable = false
        popupWindow.isFocusable = false
        popupWindow.showAtLocation(container, Gravity.CENTER,0,0)
    }
}

