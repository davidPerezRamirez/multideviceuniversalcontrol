package com.example.multideviceuniversalcontrol.controller.factory.model

enum class ABMType {
    INSERT,
    UPDATE,
    DELETE
}