package com.example.multideviceuniversalcontrol.controller.adapter

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.multideviceuniversalcontrol.controller.network.NetworkChecker
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.controller.ui.fragment.*
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType

class ViewPagerAdapter(
    arduinoData: ArduinoData, 
    fm: FragmentActivity,
    networkChecker: NetworkChecker,
    private val deviceTypes: List<DeviceType>
    ) : FragmentStateAdapter(fm) {

    private val itemList = mapOf(
        DeviceTypeConstants.TELEVISION to TelevisionDeviceFragment(arduinoData, networkChecker),
        DeviceTypeConstants.AUDIO to AudioSystemDeviceFragment(arduinoData, networkChecker),
        DeviceTypeConstants.AIR_CONDITIONER to ConditionerAirFragment(arduinoData, networkChecker)
    )

    override fun createFragment(position: Int): BaseFragment {
        return getItems()[position]
    }

    private fun getItems(): List<BaseFragment> {
        return deviceTypes.map {
            itemList[it.name]!!
        }
    }

    override fun getItemCount(): Int {
        return deviceTypes.size
    }

}