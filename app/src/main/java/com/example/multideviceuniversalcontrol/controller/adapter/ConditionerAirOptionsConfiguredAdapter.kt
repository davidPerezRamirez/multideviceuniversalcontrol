package com.example.multideviceuniversalcontrol.controller.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirOptionConfiguration

class ConditionerAirOptionsConfiguredAdapter(private val dataSet: List<ConditionerAirOptionConfiguration>) :
    RecyclerView.Adapter<ConditionerAirOptionsConfiguredAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mode: TextView = view.findViewById(R.id.tv_mode)
        val speed: TextView = view.findViewById(R.id.tv_speed)
        val temp: TextView = view.findViewById(R.id.tv_temperature)

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.button_ac_option_configuration_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val conditionerAirOptionConfiguration = dataSet[position]

        viewHolder.mode.text = conditionerAirOptionConfiguration.mode
        viewHolder.speed.text = conditionerAirOptionConfiguration.speed
        viewHolder.temp.text = conditionerAirOptionConfiguration.temperature.toString()
    }

    override fun getItemCount() = dataSet.size

}