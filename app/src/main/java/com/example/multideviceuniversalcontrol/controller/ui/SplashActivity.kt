package com.example.multideviceuniversalcontrol.controller.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.factory.PopUpFactory
import com.example.multideviceuniversalcontrol.infraestructure.network.RetrofitHttp
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.controller.ui.viewmodel.SaveUserViewModel
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm.WebServerRepositoryImpl
import com.google.android.gms.common.SignInButton
import lazyFindViewById

class SplashActivity : AppCompatActivity() {

    private val btnSignIn by lazyFindViewById<SignInButton>(R.id.sign_in_button)
    private val container by lazyFindViewById<ConstraintLayout>(R.id.splash_activity_container)

    private val viewModel: SaveUserViewModel by lazy {
        val repository = WebServerRepositoryImpl(RetrofitHttp.getRetrofitConnection())

        SaveUserViewModel(repository)
    }
    private val firebaseService: FirebaseService by lazy {
        FirebaseService(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        viewModel.saveUser.observe(this, ::observerResultSaveUser)

        nextView()
    }

    private fun observerResultSaveUser(response: Result<ResponseArduino>) {
        when (response) {
            is Result.Error -> PopUpFactory.showError(container)
            is Result.Success -> Log.i("Splash Activity", response.data.status)
        }
    }

    private fun nextView() {
        btnSignIn.visibility = View.GONE
        if (firebaseService.isUserLogger()) {
            val userId = firebaseService.getUserId()

            userId?.let {
                handleUserLogged(it)
            }
        } else {
            btnSignIn.visibility = View.VISIBLE
            btnSignIn.setSize(SignInButton.SIZE_WIDE)
            btnSignIn.setOnClickListener {
                signIn()
            }
        }
    }

    private fun goToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun signIn() {
        val signInIntent: Intent = firebaseService.getSignInClient().signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            firebaseService.handleSignInResult(data, ::handleUserLogged)
        }
    }

    private fun handleUserLogged(userId: String) {
        viewModel.saveUserId(userId)
        goToMainActivity()
    }

    companion object {

        private const val RC_SIGN_IN = 123
    }
}