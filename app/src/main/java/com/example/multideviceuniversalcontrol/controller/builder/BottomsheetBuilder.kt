package com.example.multideviceuniversalcontrol.controller.builder

import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.view.children
import com.example.multideviceuniversalcontrol.R

object BottomsheetBuilder {

    fun loadButtons(
        viewGroup: ViewGroup,
        buttonsBottomsheet: List<String>,
        nameDeviceSelected: String,
        onClickDeviceBottomSheet: (nameDevice: String) -> Unit
    ) {
        viewGroup.removeAllViews()
        buttonsBottomsheet.forEach{ device ->
            val buttonBottomSheet = View.inflate(viewGroup.context, R.layout.button_bottom_sheet, null)
            val nameDevice = buttonBottomSheet.findViewById<TextView>(R.id.tv_name_device)

            nameDevice.text = device
            buttonBottomSheet.setOnClickListener {
                val animation = AnimationUtils.loadAnimation(buttonBottomSheet.context, R.anim.scale_animation)

                onClickDeviceBottomSheet(device)
                buttonBottomSheet.startAnimation(animation)
                updateBackgroundChildren(viewGroup, buttonBottomSheet)
            }
            if (device == nameDeviceSelected) {
                updateBackgroundChildren(viewGroup, buttonBottomSheet)
            }
            viewGroup.addView(buttonBottomSheet)
        }
    }

    private fun updateBackgroundChildren(viewGroup: ViewGroup, btnSelected: View) {
        viewGroup.children.toList().forEach {
            it.background.setTint(ContextCompat.getColor(it.context, R.color.grey_700))
        }
        btnSelected.background.setTint(ContextCompat.getColor(btnSelected.context, R.color.orange_500))
    }

}