package com.example.multideviceuniversalcontrol.controller.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.multideviceuniversalcontrol.domain.SendNECSignalResponse
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.DeviceRepository
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.utils.Result
import kotlinx.coroutines.launch

class DeviceViewModel(
    val deviceRepository: DeviceRepository,
    private val preferencesRepository: PreferencesRepository
) : ViewModel() {

    private var _resultSendSignal = MutableLiveData<Result<SendNECSignalResponse>>()
    val resultSendSignal: LiveData<Result<SendNECSignalResponse>> get() = _resultSendSignal

    fun sendSignal(idDevice: Int, nameButton: String) {
        viewModelScope.launch {
            _resultSendSignal.postValue(deviceRepository.sendSignal(idDevice, nameButton))
        }
    }

    fun saveCodeInCloud(onFail: () -> Unit, idDevice: Int, nameButton: String) {
        viewModelScope.launch {
            deviceRepository.saveCodeInCloud(onFail, idDevice, nameButton)
        }
    }

    fun getIdDeviceSelected(deviceTypeName: String): Int {
        return preferencesRepository.getInt(deviceTypeName)
    }

}