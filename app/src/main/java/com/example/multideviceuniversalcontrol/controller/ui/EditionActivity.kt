package com.example.multideviceuniversalcontrol.controller.ui

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.adapter.ConditionerAirOptionsConfiguredAdapter
import com.example.multideviceuniversalcontrol.controller.adapter.ButtonsDeviceConfiguredAdapter
import com.example.multideviceuniversalcontrol.controller.factory.FileRepository
import com.example.multideviceuniversalcontrol.controller.factory.PopUpFactory
import com.example.multideviceuniversalcontrol.controller.factory.SnackBarFactory
import com.example.multideviceuniversalcontrol.controller.ui.viewmodel.EditionViewModel
import com.example.multideviceuniversalcontrol.domain.InsertStatus
import com.example.multideviceuniversalcontrol.domain.SaveCodeResult
import com.example.multideviceuniversalcontrol.domain.SignalInfo
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ChangeEditionResponse
import com.example.multideviceuniversalcontrol.infraestructure.network.RetrofitHttp
import com.example.multideviceuniversalcontrol.infraestructure.repository.StatusMode
import com.example.multideviceuniversalcontrol.infraestructure.repository.application.EditionDeviceRepositoryImpl
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.*
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.lazyFindViewById
import com.google.android.material.bottomsheet.BottomSheetBehavior

class EditionActivity : AppCompatActivity() {

    private lateinit var arduinoData: ArduinoData

    private val viewModel by lazy {
        val fileRepository = FileRepository(this)
        val repository = EditionDeviceRepositoryImpl(arduinoData)

        EditionViewModel(repository, fileRepository)
    }
    private var isShowMenu = false

    private val container by lazyFindViewById<ConstraintLayout>(R.id.edition_container)
    private val devicesSpinner by lazyFindViewById<Spinner>(R.id.sp_devices)
    private val buttonTypesSpinner by lazyFindViewById<Spinner>(R.id.sp_button_type)

    private val modeSpinner by lazyFindViewById<Spinner>(R.id.sp_mode)
    private val speedSpinner by lazyFindViewById<Spinner>(R.id.sp_speed)
    private val temperatureSpinner by lazyFindViewById<Spinner>(R.id.sp_temperature)

    private val btnSave by lazyFindViewById<Button>(R.id.btn_save)
    private val btnFinish by lazyFindViewById<Button>(R.id.btn_finish)

    private val optionsEditionsAc by lazyFindViewById<LinearLayout>(R.id.option_edition_ac)
    private val optionsEditionBaseDevice by lazyFindViewById<LinearLayout>(R.id.option_edition_base_device)

    private val addMenuItem by lazyFindViewById<ConstraintLayout>(R.id.add_device_menu_item)
    private val editMenuItem by lazyFindViewById<ConstraintLayout>(R.id.edit_device_menu_item)
    private val deleteMenuItem by lazyFindViewById<ConstraintLayout>(R.id.delete_device_menu_item)
    private val menuEdition by lazyFindViewById<ImageView>(R.id.menu_edition)

    private val buttonsConfiguredList by lazyFindViewById<RecyclerView>(R.id.list_buttons_configured)
    private val editBottonSheetTitle by lazyFindViewById<TextView>(R.id.tx_buttons_configured)
    private lateinit var editBottomSheet: ConstraintLayout
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edition)

        arduinoData = RetrofitHttp.getRetrofitConnection()
        initializeObservers()

        viewModel.getDevices()
        viewModel.changeEditionMode(StatusMode.ACTIVE)

        editBottomSheet = findViewById(R.id.buttons_configured_container)
        bottomSheetBehavior = BottomSheetBehavior.from(editBottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        initializeShowBottomSheet()

        btnSave.setOnClickListener {
            val device = devicesSpinner.selectedItem as Device

            saveCode(device)
        }

        btnFinish.setOnClickListener {
            finishActivity()
            viewModel.createDataBaseBackup()
        }
        initializeFabMenu()
        container.setOnClickListener {
            if(isShowMenu) {
                closeFABMenu()
            }
        }
    }

    private fun initializeObservers() {
        viewModel.devices.observe(this, ::observeDevices)
        viewModel.buttonByDeviceView.observe(this, ::observeButtonsByDevice)
        viewModel.temperatures.observe(this, ::observeTemperatures)
        viewModel.speeds.observe(this, ::observeSpeeds)
        viewModel.modes.observe(this, ::observeModes)
        viewModel.resultChangeEditionMode.observe(this, ::observerResultChangeEditionMode)
        viewModel.abmDevice.observe(this, ::observeChangeDevice)
        viewModel.resultSaveCodeCode.observe(this, ::observeSaveCode)
    }

    private fun initializeFabMenu() {
        closeFABMenu()
        menuEdition.setOnClickListener {
            if (isShowMenu) {
                isShowMenu = false
                closeFABMenu()
            } else {
                isShowMenu = true
                showFABMenu()
            }
        }
        addMenuItem.setOnClickListener {
            PopUpFactory.addDevice(container, viewModel.getDeviceTypes(), viewModel::addDevice)
            closeFABMenu()
        }
        editMenuItem.setOnClickListener {
            PopUpFactory.editDevice(container, viewModel.devices(), viewModel::editDevice)
            closeFABMenu()
        }
        deleteMenuItem.setOnClickListener {
            PopUpFactory.deleteDevice(container, viewModel.devices(), viewModel::deleteDevice)
            closeFABMenu()
        }
    }

    private fun initializeConfiguredButton(device: Device) {
        val buttonsConfigured = viewModel.buttonsConfigured(device)

        if (buttonsConfigured.isNotEmpty()) {
            editBottomSheet.visibility = View.VISIBLE
            val customAdapter = if (viewModel.isconditionerAir(device)) {
                ConditionerAirOptionsConfiguredAdapter(buttonsConfigured as List<ConditionerAirOptionConfiguration>)
            } else {
                ButtonsDeviceConfiguredAdapter(buttonsConfigured as List<String>)
            }
            val linearLayoutManager = LinearLayoutManager(this)
            val dividerItemDecoration = DividerItemDecoration(
                this, linearLayoutManager.orientation
            )
            buttonsConfiguredList.apply {
                layoutManager = linearLayoutManager
                adapter = customAdapter
                addItemDecoration(dividerItemDecoration)
            }
        } else {
            editBottomSheet.visibility = View.GONE
        }
    }

    private fun initializeShowBottomSheet() {
        editBottonSheetTitle.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    private fun observeChangeDevice(status: InsertStatus) {
        when(status) {
            InsertStatus.INSERTED,
            InsertStatus.UPDATED,
            InsertStatus.DELETE -> {
                viewModel.getDevices()
                closeFABMenu()
            }
            InsertStatus.ABORTED -> SnackBarFactory.createErrorSnackBar(container,
                msg = "El dispositivo ya existe o el nombre ingresado es inválido"
            )
        }

    }

    private fun showFABMenu() {
        updateActionMenu(addMenuItem, View.VISIBLE, R.dimen.standard_70)
        updateActionMenu(editMenuItem, View.VISIBLE, R.dimen.standard_126)
        updateActionMenu(deleteMenuItem, View.VISIBLE, R.dimen.standard_182)
    }

    private fun closeFABMenu() {
        updateActionMenu(addMenuItem, View.INVISIBLE, R.dimen.standard_0)
        updateActionMenu(editMenuItem, View.INVISIBLE, R.dimen.standard_0)
        updateActionMenu(deleteMenuItem, View.INVISIBLE, R.dimen.standard_0)
    }

    private fun updateActionMenu(
        container: View,
        visibility: Int,
        translationY: Int
    ) {
        container.animate().translationY(resources.getDimension(translationY))
        container.visibility = visibility
    }

    private fun saveCode(device: Device) {
        val signalInfo = if (!viewModel.isconditionerAir(device)) {
            val deviceButton = buttonTypesSpinner.selectedItem as ButtonByDeviceView

            SignalInfo(
                idDevice = device.idDevice,
                extraData = mapOf(
                    ButtonContants.NAME_BUTTON_KEY to deviceButton.nameButton
                )
            )
        } else {
            val temperature = temperatureSpinner.selectedItem as ConditionerAirTemperature
            val fan = speedSpinner.selectedItem as ConditionerAirSpeed
            val mode = modeSpinner.selectedItem as ConditionerAirMode

            SignalInfo(
                idDevice = device.idDevice,
                extraData = mapOf(
                    ButtonContants.AC_TEMPERATURE_KEY to temperature.value,
                    ButtonContants.AC_MODE_KEY to mode.name,
                    ButtonContants.AC_SPEED_KEY to fan.description
                )
            )
        }

        viewModel.saveSignalCode(signalInfo)
    }

    private fun finishActivity() {
        viewModel.changeEditionMode(StatusMode.DESACTIVE)
        this.finish()
    }

    private fun observeDevices(devices: List<Device>) {
        initializeSpinner(devicesSpinner, devices)
        devicesSpinner.setSelection(0)

        devicesSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapter: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                val device = adapter!!.getItemAtPosition(pos) as Device

                initializeEditionOptions(device)
                initializeConfiguredButton(device)
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
    }

    private fun initializeEditionOptions(device: Device) {
        if (viewModel.isconditionerAir(device)) {
            optionsEditionsAc.visibility = View.VISIBLE
            optionsEditionBaseDevice.visibility = View.GONE

            viewModel.getTemperatures()
            viewModel.getModes()
            viewModel.getSpeeds()
        } else {
            optionsEditionsAc.visibility = View.GONE
            optionsEditionBaseDevice.visibility = View.VISIBLE

            viewModel.getButtonsByDevice(device.idDeviceType)
        }
    }

    private fun observeButtonsByDevice(deviceButtons: List<ButtonByDeviceView>) {
        val adapter = ArrayAdapter(
            this,
            R.layout.item_edition_spinner,
            R.id.item_spinner,
            deviceButtons.sortedBy { it.nameButton }
        )

        buttonTypesSpinner.adapter = adapter
    }

    private fun observeTemperatures(temperatures: List<ConditionerAirTemperature>) {
        initializeSpinner(temperatureSpinner, temperatures)
    }

    private fun observeSpeeds(speeds: List<ConditionerAirSpeed>) {
        initializeSpinner(speedSpinner, speeds)
    }

    private fun observeModes(modes: List<ConditionerAirMode>) {
        initializeSpinner(modeSpinner, modes)
    }

    private fun<T> initializeSpinner(spinner: Spinner, items: List<T>) {
        val adapter = ArrayAdapter(this, R.layout.item_edition_spinner, R.id.item_spinner, items)

        spinner.adapter = adapter
        if(isShowMenu) {
            closeFABMenu()
        }
    }

    private fun observerResultChangeEditionMode(response: Result<ChangeEditionResponse>) {
        when (response) {
            is Result.Error -> PopUpFactory.showError(container)
            is Result.Success -> SnackBarFactory.createSuccessSnackBar(container,"Modo edición activado")
        }
    }

    private fun observeSaveCode(response: Result<SaveCodeResult>) {
        when (response) {
            is Result.Error -> PopUpFactory.showError(container)
            is Result.Success -> {
                initializeConfiguredButton(response.data.device)
                SnackBarFactory.createSuccessSnackBar(container, "Se guardo el código exitosamente")
            }
        }
    }

    override fun onBackPressed() {
        finishActivity()
    }
}