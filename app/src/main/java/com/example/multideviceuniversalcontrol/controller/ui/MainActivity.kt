package com.example.multideviceuniversalcontrol.controller.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.GridLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.multideviceuniversalcontrol.R
import com.example.multideviceuniversalcontrol.controller.adapter.ViewPagerAdapter
import com.example.multideviceuniversalcontrol.controller.builder.BottomsheetBuilder
import com.example.multideviceuniversalcontrol.controller.builder.ShareViewBuilder
import com.example.multideviceuniversalcontrol.controller.factory.FileRepository
import com.example.multideviceuniversalcontrol.controller.factory.PopUpFactory
import com.example.multideviceuniversalcontrol.controller.factory.SnackBarFactory
import com.example.multideviceuniversalcontrol.controller.network.NetworkChecker
import com.example.multideviceuniversalcontrol.controller.network.NetworkType
import com.example.multideviceuniversalcontrol.controller.ui.viewmodel.MainViewModel
import com.example.multideviceuniversalcontrol.domain.StatusConnection
import com.example.multideviceuniversalcontrol.domain.StatusConnectionWifi
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.mvvm.contracts.WebServerRepository
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.infraestructure.network.RetrofitHttp
import com.example.multideviceuniversalcontrol.infraestructure.repository.DataBaseRepositoryImpl
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm.WebServerRepositoryImpl
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.utils.lazyFindViewById
import com.facebook.stetho.Stetho
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : FragmentActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var arduinoData: ArduinoData
    private val firebaseService: FirebaseService by lazy {
        FirebaseService(this)
    }
    private val webServerRepository: WebServerRepository by lazy {
        WebServerRepositoryImpl(arduinoData)
    }
    private val viewModel by lazy {
        val preferencesRepository =
            PreferencesRepository(getPreferences(MODE_PRIVATE))

        MainViewModel(preferencesRepository, DataBaseRepositoryImpl(), webServerRepository)
    }
    private val networkChecker: NetworkChecker by lazy {
        NetworkChecker(
            this.applicationContext,
            webServerRepository::checkConnection
        )
    }

    private val emptyFragment by lazyFindViewById<ScrollView>(R.id.empty_fragment)
    private val drawerLayout by lazyFindViewById<DrawerLayout>(R.id.drawer_layout)
    private val navigationView by lazyFindViewById<NavigationView>(R.id.nav_view)
    private val tabLayout by lazyFindViewById<TabLayout>(R.id.tabLayout)
    private val viewPager by lazyFindViewById<ViewPager2>(R.id.viewPager)
    private val btnEdit by lazyFindViewById<FloatingActionButton>(R.id.btnEdit)
    private val btnConfig by lazyFindViewById<FloatingActionButton>(R.id.btnConfig)
    private val bottomSheetTitle by lazyFindViewById<TextView>(R.id.tv_change_device)
    private val bottomsheetContainer by lazyFindViewById<GridLayout>(R.id.bottomsheet_container)

    private lateinit var bottomSheet: ConstraintLayout
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private lateinit var viewPagerAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Stetho.initializeWithDefaults(this)
        AppDatabase.createAppDataBase(context = this)
        arduinoData = RetrofitHttp.getRetrofitConnection()

        bottomSheet = findViewById(R.id.bottom_sheet_layout)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, null, R.string.app_name,
            R.string.text_button_close)

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)

        viewModel.checkConnection.observe(this, ::observeCheckConnection)
        viewModel.synchronize.observe(this, ::observerResultSaveUser)

        createTabLayout()
        initializeFloatingButton()
        initializeBtnChangeDevice()
    }

    private fun observeCheckConnection(statusConnection: StatusConnection) {
        when (statusConnection.status) {
            StatusConnectionWifi.DISCONNECTED -> PopUpFactory.showError(tabLayout)
            StatusConnectionWifi.CONNECTED -> SnackBarFactory.createSuccessSnackBar(tabLayout,
                msg = "Conexión exitosa")
        }
    }

    private fun observerResultSaveUser(response: Result<ResponseArduino>) {
        when (response) {
            is Result.Error -> PopUpFactory.showError(tabLayout)
            is Result.Success -> SnackBarFactory.createSuccessSnackBar(tabLayout,
                msg = "Se ha sincronizado con el servidor exitosamente")
        }
    }

    override fun onStart() {
        createTabLayout()
        super.onStart()
    }

    private fun createTabLayout() {
        val deviceTypes = viewModel.getAllDeviceTypes()

        tabLayout.removeAllTabs()
        initializeViewPager(deviceTypes)
        if (deviceTypes.isNotEmpty()) {
            emptyFragment.visibility = View.GONE
            for (type in deviceTypes) {
                tabLayout.addTab(tabLayout.newTab().setText(type.name))
            }
            tabLayout.tabGravity = TabLayout.GRAVITY_FILL

            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = deviceTypes[position].name
            }.attach()
            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    viewPager.currentItem = tab.position
                    loadButtonsDevice()
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            })
            loadButtonsDevice()
        } else {
            emptyFragment.visibility = View.VISIBLE
        }
    }

    private fun initializeViewPager(deviceTypes: List<DeviceType>) {
        viewPagerAdapter = ViewPagerAdapter(arduinoData, this, networkChecker, deviceTypes)
        viewPager.adapter = viewPagerAdapter
        viewPager.currentItem = 0
    }

    private fun loadButtonsDevice() {
        val baseFragment = viewPagerAdapter.createFragment(viewPager.currentItem)
        val deviceType = baseFragment.getDeviceTypeName()
        val devices = viewModel.getDevicesByType(deviceType)

        if (devices.isNotEmpty() && viewModel.getDeviceSelected(deviceType) < 0) {
            val device = devices.first()
            viewModel.saveDeviceSelected(device)
        }
        if (devices.size > 1) {
            bottomSheet.visibility = View.VISIBLE
            val deviceSelected = viewModel.getDeviceById(viewModel.getDeviceSelected(deviceType))
            BottomsheetBuilder.loadButtons(
                bottomsheetContainer,
                devices.map { it.name },
                deviceSelected.name,
                this::onClickDeviceBottomSheet
            )
        } else {
            bottomSheet.visibility = View.GONE
        }
    }

    private fun onClickDeviceBottomSheet(nameDevice: String) {
        val device = viewModel.getDeviceByName(nameDevice)

        viewModel.saveDeviceSelected(device)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun initializeFloatingButton() {
        btnEdit.setOnClickListener{
            val intent = Intent(this, EditionActivity::class.java)
            startActivity(intent)
        }
        btnConfig.setOnClickListener {
            drawerLayout.openDrawer(Gravity.LEFT)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val isLocalNetwork = networkChecker.getNetworkType() == NetworkType.LOCAL

        when(item.itemId) {
            R.id.nav_item_import -> {
                ShareViewBuilder.importFile(this::startActivityForResult)
            }
            R.id.nav_item_export -> {
                val file = FileRepository(this).createDatabaseBackup()
                ShareViewBuilder.exportFile(file, this, this::startActivity)
            }
            R.id.nav_item_logout -> {
                val intent = Intent(this, SplashActivity::class.java)

                firebaseService.getSignInClient().signOut()
                startActivity(intent)
            }
            R.id.nav_item_status_connection -> {
                if(isLocalNetwork) {
                    viewModel.checkArduinoConnection()
                } else {
                    SnackBarFactory.createErrorSnackBar(tabLayout,
                        msg = "No está conectado a la misma red que el servidor")
                }
            }
            R.id.nav_item_synchronize -> {
                if (isLocalNetwork) {
                    viewModel.synchronize(firebaseService.getUserId()!!)
                } else {
                    SnackBarFactory.createErrorSnackBar(tabLayout,
                        msg = "No está conectado a la misma red que el servidor")
                }
            }
            R.id.nav_item_help -> {
                val urlHelp = resources.getString(R.string.url_help)
                val uri = Uri.parse(urlHelp)

                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
        }

        drawerLayout.closeDrawer(Gravity.LEFT)

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ShareViewBuilder.IMPORT_CODE && resultCode == RESULT_OK) {
            importFileDataBase(data?.data)
        }
    }

    private fun importFileDataBase(uriSelectedFile: Uri?) {
        uriSelectedFile?.let {
            //TODO ver que hacer si no puedo obtener el inputStream
            val inputStream = contentResolver.openInputStream(it)
            inputStream?.let { input ->
                //TODO ver que hacer si no puedo recuperar los datos
                val backupDataBase = FileRepository(this).readBackupDataBase(input)
                //TODO esta fun de view model tiene que tener un observer
                //TODO al terminar de importar debe crear el tab y cargar los devices
                viewModel.importFileDataBase(backupDataBase)
                createTabLayout()
                loadButtonsDevice()

            }
        }
    }

    private fun initializeBtnChangeDevice() {
        bottomSheetTitle.setOnClickListener {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            } else {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    override fun onBackPressed() {

        if (viewPager.currentItem == 0) {
            finishAffinity()
        } else {
            viewPager.currentItem -= 1
        }
    }

}
