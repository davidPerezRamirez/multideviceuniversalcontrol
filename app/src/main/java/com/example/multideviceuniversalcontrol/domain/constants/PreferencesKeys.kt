package com.example.multideviceuniversalcontrol.domain.constants

object PreferencesKeys {

    const val AIR_COND_MODE = "AirCondMode"
    const val AIR_COND_TEMP = "AirCondTemperature"
    const val AIR_COND_SPEED = "AirCondSpeed"
    const val AIR_COND_POWER = "AirCondPower"

    const val AUDIO_BASS = "AudioBass"
    const val AUDIO_TRE = "AudioTre"
    const val AUDIO_VOLUME = "AudioVolume"

}