package com.example.multideviceuniversalcontrol.domain.arduino.model

data class StatusConnectionResponse(
    val status: String,
    val isLogged: String
)
