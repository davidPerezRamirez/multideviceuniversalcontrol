package com.example.multideviceuniversalcontrol.domain.arduino.model

data class ChangeEditionResponse(
    val status: String,
    val isModeEdition: Int
)
