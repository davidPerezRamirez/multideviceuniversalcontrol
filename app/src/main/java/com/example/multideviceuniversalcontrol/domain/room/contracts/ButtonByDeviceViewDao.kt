package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonByDeviceView

@Dao
interface ButtonByDeviceViewDao {

    @Query("SELECT * FROM ButtonByDeviceView WHERE idDeviceType = :idDeviceType")
    fun findByIdDeviceType(idDeviceType: Int): List<ButtonByDeviceView>
}