package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonByDevice

@Dao
interface ButtonByDeviceDao {

    @Query("SELECT * FROM ButtonByDevice")
    fun getAll(): List<ButtonByDevice>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(buttonsByDevice: List<ButtonByDevice>)
}