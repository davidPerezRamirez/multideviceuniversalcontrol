package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirMode

@Dao
interface ConditionerAirModeDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(deviceTypes: List<ConditionerAirMode>)

    @Query("SELECT * FROM ConditionerAirMode WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): ConditionerAirMode

    @Query("SELECT * FROM ConditionerAirMode")
    fun getAll(): List<ConditionerAirMode>
}