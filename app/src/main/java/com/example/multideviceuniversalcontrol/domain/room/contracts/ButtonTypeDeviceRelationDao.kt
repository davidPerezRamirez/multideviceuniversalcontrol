package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.*
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonTypeDeviceRelation

@Dao
interface ButtonTypeDeviceRelationDao {

    @Query("Select * from ButtonTypeDeviceRelation")
    fun getAll(): List<ButtonTypeDeviceRelation>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(buttonTypeDeviceRelations: List<ButtonTypeDeviceRelation>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(buttonTypeDeviceRelation: ButtonTypeDeviceRelation)

}