package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.*
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType

@Dao
interface DeviceTypeRepository {

    @Query("SELECT * FROM DeviceType")
    fun getAll(): List<DeviceType>

    @Query("SELECT * FROM DeviceType WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): DeviceType

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(deviceTypes: List<DeviceType>)

    @Delete
    fun delete(type: DeviceType)

    @Query("SELECT * FROM DeviceType WHERE idDeviceType = :idDeviceType")
    fun findById(idDeviceType: Int): DeviceType
}