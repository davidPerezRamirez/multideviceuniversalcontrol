package com.example.multideviceuniversalcontrol.domain

data class SendNECSignalResponse(
    val status: SendSignalStatus
)