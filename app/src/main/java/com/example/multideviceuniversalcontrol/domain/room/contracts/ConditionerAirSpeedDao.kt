package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirSpeed

@Dao
interface ConditionerAirSpeedDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(deviceTypes: List<ConditionerAirSpeed>)

    @Query("SELECT * FROM ConditionerAirSpeed WHERE description LIKE :name LIMIT 1")
    fun findByName(name: String): ConditionerAirSpeed

    @Query("SELECT * FROm ConditionerAirSpeed")
    fun getAll(): List<ConditionerAirSpeed>
}