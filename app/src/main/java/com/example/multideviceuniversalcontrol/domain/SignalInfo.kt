package com.example.multideviceuniversalcontrol.domain

data class SignalInfo(
    val idDevice: Int,
    val extraData: Map<String, *>? = null
)
