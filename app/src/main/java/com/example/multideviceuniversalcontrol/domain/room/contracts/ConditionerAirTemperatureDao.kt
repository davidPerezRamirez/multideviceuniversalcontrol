package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirTemperature

@Dao
interface ConditionerAirTemperatureDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(deviceTypes: List<ConditionerAirTemperature>)

    @Query("SELECT * FROM ConditionerAirTemperature WHERE value LIKE :value LIMIT 1")
    fun findByName(value: Int): ConditionerAirTemperature

    @Query("SELECT * FROM ConditionerAirTemperature")
    fun getAll(): List<ConditionerAirTemperature>
}