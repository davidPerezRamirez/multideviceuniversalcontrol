package com.example.multideviceuniversalcontrol.domain.arduino

import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.User
import com.example.multideviceuniversalcontrol.domain.arduino.model.ChangeEditionResponse
import com.example.multideviceuniversalcontrol.domain.arduino.model.DecodedCode
import com.example.multideviceuniversalcontrol.domain.arduino.model.StatusConnectionResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ArduinoData {

    @GET("sendNEC")
    suspend fun sendNEC(@Query("code") codeIR: String): ResponseArduino

    @POST("sendRaw")
    suspend fun sendRaw(@Query("code") codeRaw: String): ResponseArduino

    @POST("saveUserId")
    suspend fun saveUserId(@Body body: User): ResponseArduino

    @GET("changeModeEdition")
    suspend fun changeEditionMode(@Query("status") statusMode: String): ChangeEditionResponse

    @GET("getDecodedCode")
    suspend fun getDecodedMode(): DecodedCode

    @GET("checkStatusConnection")
    suspend fun checkStatusConnection(): StatusConnectionResponse

}