package com.example.multideviceuniversalcontrol.domain

enum class InsertStatus {
    INSERTED,
    ABORTED,
    DELETE,
    UPDATED
}