package com.example.multideviceuniversalcontrol.domain.mvvm.contracts

import com.example.multideviceuniversalcontrol.domain.SaveCodeResult
import com.example.multideviceuniversalcontrol.domain.SignalInfo
import com.example.multideviceuniversalcontrol.domain.arduino.model.ChangeEditionResponse
import com.example.multideviceuniversalcontrol.infraestructure.repository.StatusMode
import com.example.multideviceuniversalcontrol.utils.Result

interface EditionDeviceRepository {

    /**
     * Change mode edition web server
     */
    suspend fun changeEditionMode(statusMode: StatusMode): Result<ChangeEditionResponse>

    /**
     * POST save signal code
     */
    suspend fun saveSignalCode(signalInfo: SignalInfo): Result<SaveCodeResult>

    /**
     * determines if the device is air conditioning
     */
    fun isConditionerAir(idDeviceType: Int): Boolean
}