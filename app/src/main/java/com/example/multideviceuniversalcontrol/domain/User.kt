package com.example.multideviceuniversalcontrol.domain

data class User(
    val userId: String
)
