package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirOptionConfiguration

@Dao
interface ConditionerAirOptionConfigurationDao {

    @Query("Select * from ConditionerAirOptionConfiguration where idDevice = :idDevice")
    fun getOptionConfiguration(idDevice: Int): List<ConditionerAirOptionConfiguration>
}