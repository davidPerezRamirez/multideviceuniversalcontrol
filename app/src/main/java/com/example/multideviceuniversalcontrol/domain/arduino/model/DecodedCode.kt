package com.example.multideviceuniversalcontrol.domain.arduino.model

data class DecodedCode(
    val hexaCode: String,
    val rawCode: List<Int>
)
