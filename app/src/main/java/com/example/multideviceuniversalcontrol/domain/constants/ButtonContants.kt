package com.example.multideviceuniversalcontrol.domain.constants

object ButtonContants {

    const val ON_OFF = "apagado/encendido"
    const val VOLUMEN_UP = "Volumen +"
    const val VOLUMEN_DOWN = "Volumen -"
    const val CHANNEL_UP = "Canal +"
    const val CHANNEL_DOWN = "Canal -"
    const val MUTE = "Mute"
    const val BACK = "Back"
    const val ZERO = "0"
    const val ONE = "1"
    const val TWO = "2"
    const val THREE = "3"
    const val FOUR = "4"
    const val FIVE = "5"
    const val SIX = "6"
    const val SEVEN = "7"
    const val EIGHT = "8"
    const val NINE = "9"
    const val PLAY = "play"
    const val PAUSE = "pause"
    const val UP = "up"
    const val DOWN = "down"
    const val OK = "ok"
    const val RIGHT = "right"
    const val LEFT = "left"
    const val HOME = "home"
    const val BASS_UP = "BASS +"
    const val BASS_DOWN = "BASS -"
    const val TREBLE_UP = "TREBLE +"
    const val TREBLE_DOWN = "TREBLE -"
    const val COOL_MODE = "Cool mode"
    const val HEAT_MODE = "Heat mode"
    const val TEMP_UP = "Temperature +"
    const val TEMP_DOWN = "Temperature -"
    const val SPEED_LOW = "Speed low"
    const val SPEED_NORMAL = "Speed normal"
    const val SPEED_HIGH = "Speed high"

    const val NAME_BUTTON_KEY = "name_button"
    const val AC_MODE_KEY = "mode"
    const val AC_SPEED_KEY = "speed"
    const val AC_TEMPERATURE_KEY = "temperature"
}
