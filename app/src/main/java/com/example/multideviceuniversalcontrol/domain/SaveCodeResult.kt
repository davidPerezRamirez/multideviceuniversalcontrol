package com.example.multideviceuniversalcontrol.domain

import com.example.multideviceuniversalcontrol.domain.arduino.model.DecodedCode
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.Device

data class SaveCodeResult(
    val device: Device,
    val decodedCode: DecodedCode
)
