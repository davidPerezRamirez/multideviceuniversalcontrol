package com.example.multideviceuniversalcontrol.domain

data class SendRawSignalResponse(
    val status: SendSignalStatus,
    val isOffButton: Boolean = false
)