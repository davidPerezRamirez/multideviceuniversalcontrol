package com.example.multideviceuniversalcontrol.domain.network.contracts

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceType

interface DataBaseRepository {

    suspend fun getDeviceTypesEnabled(): List<DeviceType>
}