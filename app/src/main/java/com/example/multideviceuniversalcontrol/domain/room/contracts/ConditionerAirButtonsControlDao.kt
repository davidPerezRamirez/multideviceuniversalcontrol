package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirButtonControl

@Dao
interface ConditionerAirButtonsControlDao {

    @Query("Select * from ConditionerAirButtonControl where idDevice = :idDevice AND mode LIKE :mode AND temperature LIKE :temp AND speed LIKE :speed")
    fun findButton(idDevice: Int, mode: String, temp: Int, speed: String): ConditionerAirButtonControl?

    @Query("Select * from ConditionerAirButtonControl where idDevice = :idDevice AND mode LIKE \"OFF\"")
    fun findOffButton(idDevice: Int): ConditionerAirButtonControl?

}
