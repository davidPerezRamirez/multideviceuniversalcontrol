package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.*
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.Device

@Dao
interface DeviceDao {

    @Query("SELECT * FROM Device WHERE idDeviceType = :idDeviceType")
    fun getAllDeviceByType(idDeviceType: Int): List<Device>

    @Query("SELECT * FROM Device WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): Device

    @Query("SELECT * FROM Device WHERE idDevice = :idDevice LIMIT 1")
    fun findById(idDevice: Int): Device

    @Query("SELECT * FROM Device")
    fun getAll(): List<Device>

    @Delete
    fun delete(button: Device): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(devices: List<Device>)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(newDevice: Device): Long

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun updateNameDevice(device: Device): Int
}