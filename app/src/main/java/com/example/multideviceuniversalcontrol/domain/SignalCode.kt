package com.example.multideviceuniversalcontrol.domain

data class SignalCode(
    val nameDevice: String,
    val hexaCode: String,
    val rawCode: List<Int>,
    val extraData: Map<String, *>? = null
)
