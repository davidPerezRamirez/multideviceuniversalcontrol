package com.example.multideviceuniversalcontrol.domain.mvvm.contracts

import com.example.multideviceuniversalcontrol.domain.StatusConnection
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.utils.Result

interface WebServerRepository {

    suspend fun saveUserId(userId: String): Result<ResponseArduino>

    suspend fun checkConnection(): StatusConnection
}