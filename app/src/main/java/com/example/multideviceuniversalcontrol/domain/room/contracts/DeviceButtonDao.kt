package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.*
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.DeviceButton

@Dao
interface DeviceButtonDao {

    @Query("SELECT * FROM DeviceButton")
    fun getAll(): List<DeviceButton>

    @Query("SELECT * FROM DeviceButton WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): DeviceButton

    @Delete
    fun delete(button: DeviceButton)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(deviceButtons: List<DeviceButton>)
}