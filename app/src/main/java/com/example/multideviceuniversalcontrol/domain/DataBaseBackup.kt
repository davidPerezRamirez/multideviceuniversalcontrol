package com.example.multideviceuniversalcontrol.domain

import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirButtonRelation
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonTypeDeviceRelation
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.Device

data class DataBaseBackup(
    val devices: List<Device>,
    val deviceData: List<ButtonTypeDeviceRelation>,
    val conditionedAirData: List<ConditionerAirButtonRelation>
)
