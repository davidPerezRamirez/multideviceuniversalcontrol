package com.example.multideviceuniversalcontrol.domain.constants

object DeviceTypeConstants {

    const val TELEVISION = "Television"
    const val AUDIO = "Audio"
    const val AIR_CONDITIONER = "Aire Ac."
}