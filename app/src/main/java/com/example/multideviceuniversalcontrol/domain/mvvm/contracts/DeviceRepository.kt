package com.example.multideviceuniversalcontrol.domain.mvvm.contracts

import com.example.multideviceuniversalcontrol.domain.SendNECSignalResponse
import com.example.multideviceuniversalcontrol.utils.Result

interface DeviceRepository {

    suspend fun sendSignal(idDevice: Int, nameButton: String): Result<SendNECSignalResponse>

    suspend fun saveCodeInCloud(onFail: () -> Unit, idDevice: Int, nameButton: String)
}