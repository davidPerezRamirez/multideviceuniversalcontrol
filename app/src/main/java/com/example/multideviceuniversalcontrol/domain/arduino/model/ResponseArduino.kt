package com.example.multideviceuniversalcontrol.domain.arduino.model

data class ResponseArduino(val status: String)