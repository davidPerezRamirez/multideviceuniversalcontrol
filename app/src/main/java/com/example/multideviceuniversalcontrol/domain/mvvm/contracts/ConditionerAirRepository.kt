package com.example.multideviceuniversalcontrol.domain.mvvm.contracts

import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.domain.SendRawSignalResponse

interface ConditionerAirRepository {

    suspend fun sendConditionerAirSignal(
        idDevice: Int,
        mode: String,
        temp: Int,
        speed: String
    ): Result<SendRawSignalResponse>

    suspend fun sendConditionerAirOffSignal(idDevice: Int): Result<SendRawSignalResponse>

    suspend fun saveCodeInCloud(
        onFail: () -> Unit,
        idDevice: Int,
        mode: String? = null,
        temp: Int? = null,
        speed: String? = null
    )
}