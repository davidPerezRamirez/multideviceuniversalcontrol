package com.example.multideviceuniversalcontrol.domain.constants

object PreferencesValues {

    /**
     * Conditioner Air Speeds
     */
    const val LOW_SPEED = "Min"
    const val NORMAL_SPEED = "Normal"
    const val HIGH_SPEED = "Max"

    /**
     * Conditioner Air Modes
     */
    const val COOL_MODE = "Frío"
    const val HEAT_MODE = "Calor"

    /**
     * Conditioner Air Power
     */
    const val POWER_ON = "On"
    const val POWER_OFF = "Off"

}