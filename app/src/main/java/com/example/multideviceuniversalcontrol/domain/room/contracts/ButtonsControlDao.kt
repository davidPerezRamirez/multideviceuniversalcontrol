package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonsControl

@Dao
interface ButtonsControlDao {

    @Query("Select * from ButtonsControl where idDevice = :idDevice AND nameButton LIKE :nameButton")
    fun findButtonFromDevice(idDevice: Int, nameButton: String): ButtonsControl?

    @Query("Select * from ButtonsControl where idDevice = :idDevice")
    fun getButtonsDevice(idDevice: Int): List<ButtonsControl>
}