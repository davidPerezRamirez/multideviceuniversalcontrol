package com.example.multideviceuniversalcontrol.domain

import java.util.*

enum class SendSignalStatus {
    SUCCESS,
    NOT_FOUND,
    FAILURE;

    companion object {
        fun from (status: String): SendSignalStatus {
            return try {
                enumValueOf(status.toUpperCase(Locale.ROOT))
            } catch (ex: Exception) {
                FAILURE
            }
        }
    }
}