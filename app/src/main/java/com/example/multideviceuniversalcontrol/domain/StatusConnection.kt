package com.example.multideviceuniversalcontrol.domain

import java.util.*

data class StatusConnection(
    val status: StatusConnectionWifi = StatusConnectionWifi.DISCONNECTED,
    val isLogged: Boolean
)

enum class StatusConnectionWifi {
    CONNECTED,
    DISCONNECTED;

    companion object {
        fun from (status: String): StatusConnectionWifi {
            return try {
                enumValueOf(status.toUpperCase(Locale.ROOT))
            } catch (ex: Exception) {
                DISCONNECTED
            }
        }
    }
}
