package com.example.multideviceuniversalcontrol.domain.room.contracts

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirButtonRelation

@Dao
interface ConditionerAirButtonRelationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(deviceTypes: List<ConditionerAirButtonRelation>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(deviceTypes: ConditionerAirButtonRelation)

    @Query("Select * from ConditionerAirButtonRelation")
    fun getAll(): List<ConditionerAirButtonRelation>
}