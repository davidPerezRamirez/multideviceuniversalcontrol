package com.example.multideviceuniversalcontrol.infraestructure.repository.application

import com.example.multideviceuniversalcontrol.domain.SaveCodeResult
import com.example.multideviceuniversalcontrol.domain.SignalInfo
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ChangeEditionResponse
import com.example.multideviceuniversalcontrol.domain.arduino.model.DecodedCode
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.infraestructure.repository.StatusMode
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.*
import com.example.multideviceuniversalcontrol.utils.ErrorEntity
import com.example.multideviceuniversalcontrol.utils.Result
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@Suppress("UNCHECKED_CAST")
class EditionDeviceRepositoryTest {

    private val mockService = mockk<ArduinoData>()
    private val mockDataBase = mockk<AppDatabase>()
    private val editionDeviceRepository = EditionDeviceRepositoryImpl(mockService)

    private lateinit var result: Result<*>
    private lateinit var signalInfo: SignalInfo

    @BeforeEach
    fun setUp() {
        AppDatabase.INSTANCE = mockDataBase
    }

    @Test
    fun `change mode edition active then change mode successfully`() {
        givenActiveModeEdition()

        whenChangeModeEdition()

        thenModeEditionChangedToActive()
    }

    @Test
    fun `change mode edition active fail then return error`() {
        givenServiceFails()

        whenChangeModeEdition()

        thenReturnError()
    }

    @Test
    fun `save signal code from device in local data base`() {
        givenValidSignalFromDeviceToSave()
        givenValidDecodedCodeFromDevice()

        whenSaveSignalCodeInLocalDataBase()

        thenSaveSignalCodeFromDeviceSuccessfully()
    }

    @Test
    fun `save signal code from conditioner air in local data base`() {
        givenValidSignalFromConditionerAirToSave()
        givenValidDecodedCodeFromDevice()

        whenSaveSignalCodeInLocalDataBase()

        thenSaveSignalCodeFromConditionerAirSuccessfully()
    }

    @Test
    fun `save signal code fail then return error`() {
        givenValidSignalFromDeviceToSave()
        givenGetDecodedCodeFromDeviceFail()

        whenSaveSignalCodeInLocalDataBase()

        thenReturnError()
    }

    private fun givenActiveModeEdition() {
        coEvery {
            mockService.changeEditionMode(any())
        } returns ChangeEditionResponse(
            status = "SUCCESS",
            isModeEdition = 1
        )
    }

    private fun givenGetDecodedCodeFromDeviceFail() {
        coEvery {
            mockService.changeEditionMode(any())
        } throws Exception("Algo salió mal")
    }

    private fun givenValidSignalFromDeviceToSave() {
        signalInfo = SignalInfo(
            idDevice = 1,
            extraData = mapOf(
                ButtonContants.NAME_BUTTON_KEY to "Encendido/Apagado"
            )
        )

        every {
            mockDataBase.deviceRepository().findById(signalInfo.idDevice)
        } returns Device(
            idDevice = 1,
            idDeviceType = 1,
            name = "Tv"
        )

        every {
            mockDataBase.deviceTypeRepository().findById(1)
        } returns DeviceType(
            idDeviceType = 1,
            name = DeviceTypeConstants.TELEVISION
        )

        every {
            mockDataBase.deviceButtonRepository().findByName("Encendido/Apagado")
        } returns DeviceButton(
            idDeviceButton = 1,
            name = "Encendido/Apagado"
        )

        justRun { mockDataBase.buttonTypeDeviceRelationRepository().insert(any()) }
    }

    private fun givenValidSignalFromConditionerAirToSave() {
        signalInfo = SignalInfo(
            idDevice = 2,
            extraData = mapOf(
                ButtonContants.AC_TEMPERATURE_KEY to 24,
                ButtonContants.AC_MODE_KEY to "cool",
                ButtonContants.AC_SPEED_KEY to "min"
            )
        )

        every {
            mockDataBase.deviceRepository().findById(signalInfo.idDevice)
        } returns Device(
            idDevice = 2,
            idDeviceType = 3,
            name = "Aire Comedor"
        )

        every {
            mockDataBase.deviceTypeRepository().findById(3)
        } returns DeviceType(
            idDeviceType = 3,
            name = DeviceTypeConstants.AIR_CONDITIONER
        )

        every {
            mockDataBase.conditionerAirSpeedRepository().findByName("min")
        } returns ConditionerAirSpeed(
            idSpeed = 1,
            description = "min"
        )

        every {
            mockDataBase.conditionerAirModeRepository().findByName("cool")
        } returns ConditionerAirMode(
            idMode = 1,
            name = "cool"
        )

        every {
            mockDataBase.conditionerAirTemperatureRepository().findByName(24)
        } returns ConditionerAirTemperature(
            idTemperature = 1,
            value = 24
        )

        justRun { mockDataBase.conditionerAirButtonRelationRepository().insert(any()) }
    }

    private fun givenValidDecodedCodeFromDevice() {
        coEvery {
            mockService.getDecodedMode()
        } returns DecodedCode(
            hexaCode = "0x1234",
            rawCode = listOf(1,2,3,4)
        )
    }

    private fun givenServiceFails() {
        coEvery {
            mockService.changeEditionMode(any())
        } throws Exception("Algo salió mal")
    }

    private fun whenChangeModeEdition() {
        result = runBlocking {
            editionDeviceRepository.changeEditionMode(StatusMode.ACTIVE)
        }
    }

    private fun whenSaveSignalCodeInLocalDataBase() {
        result = runBlocking {
            editionDeviceRepository.saveSignalCode(signalInfo)
        }
    }

    private fun thenModeEditionChangedToActive() {
        assertTrue(result is Result.Success<*>)

        val response = result as Result.Success<ChangeEditionResponse>
        assertEquals("SUCCESS", response.data.status)
    }

    private fun thenSaveSignalCodeFromDeviceSuccessfully() {
        assertTrue(result is Result.Success<*>)

        val response = result as Result.Success<SaveCodeResult>
        assertEquals("0x1234", response.data.decodedCode.hexaCode)
        assertEquals(1, response.data.device.idDevice)
        assertEquals(1, response.data.device.idDeviceType)
        assertEquals("Tv", response.data.device.name)

        verify(exactly = 1) { mockDataBase.buttonTypeDeviceRelationRepository().insert(any()) }
    }

    private fun thenSaveSignalCodeFromConditionerAirSuccessfully() {
        assertTrue(result is Result.Success<*>)

        val response = result as Result.Success<SaveCodeResult>
        assertEquals(listOf(1,2,3,4), response.data.decodedCode.rawCode)
        assertEquals(2, response.data.device.idDevice)
        assertEquals(3, response.data.device.idDeviceType)
        assertEquals("Aire Comedor", response.data.device.name)

        verify(exactly = 1) { mockDataBase.conditionerAirButtonRelationRepository().insert(any()) }
    }

    private fun thenReturnError() {
        assertTrue(result is Result.Error<*>)

        val response = result as Result.Error<ErrorEntity>
        assertTrue(response.error is ErrorEntity.Unknown)
    }
}