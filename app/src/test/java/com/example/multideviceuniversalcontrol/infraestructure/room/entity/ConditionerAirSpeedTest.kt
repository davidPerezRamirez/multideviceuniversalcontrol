package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HEAT_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HIGH_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.LOW_SPEED
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.NORMAL_SPEED
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ConditionerAirSpeedTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        db = AppDatabase.createAppDataBase(context)!!
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `test quantity conditioner air mde return 4`() {
        val conditionerAirSpeeds = db.conditionerAirSpeedRepository().getAll()

        assertEquals(4, conditionerAirSpeeds.size)
    }

    @Test
    fun `test conditioner air speed normal`() {
        val conditionerAirSpeed = db.conditionerAirSpeedRepository().findByName(NORMAL_SPEED)

        assertNotNull(conditionerAirSpeed)
        assertEquals("Normal", conditionerAirSpeed.description)
    }
    @Test
    fun `test conditioner air speed min`() {
        val conditionerAirSpeed = db.conditionerAirSpeedRepository().findByName(LOW_SPEED)

        assertNotNull(conditionerAirSpeed)
        assertEquals("Min", conditionerAirSpeed.description)
    }

    @Test
    fun `test conditioner air speed max`() {
        val conditionerAirSpeed = db.conditionerAirSpeedRepository().findByName(HIGH_SPEED)

        assertNotNull(conditionerAirSpeed)
        assertEquals("Max", conditionerAirSpeed.description)
    }

    @Test
    fun `test conditioner air speed void`() {
        val conditionerAirSpeed = db.conditionerAirSpeedRepository().findByName("Sin viento")

        assertNotNull(conditionerAirSpeed)
        assertEquals("Sin viento", conditionerAirSpeed.description)
    }
}