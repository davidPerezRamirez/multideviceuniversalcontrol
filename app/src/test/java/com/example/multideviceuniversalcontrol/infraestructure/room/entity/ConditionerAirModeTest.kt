package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.COOL_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HEAT_MODE
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ConditionerAirModeTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        db = AppDatabase.createAppDataBase(context)!!
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `test quantity conditioner air mde return 2`() {
        val conditionerAirModes = db.conditionerAirModeRepository().getAll()

        assertEquals(3, conditionerAirModes.size)
    }

    @Test
    fun `test conditioner air mode cool`() {
        val conditionerAirMode = db.conditionerAirModeRepository().findByName(COOL_MODE)

        assertNotNull(conditionerAirMode)
        assertEquals("Frío", conditionerAirMode.name)
    }

    @Test
    fun `test conditioner air mode heat`() {
        val conditionerAirMode = db.conditionerAirModeRepository().findByName(HEAT_MODE)

        assertNotNull(conditionerAirMode)
        assertEquals("Calor", conditionerAirMode.name)
    }

    @Test
    fun `test conditioner air mode off`() {
        val conditionerAirMode = db.conditionerAirModeRepository().findByName("OFF")

        assertNotNull(conditionerAirMode)
        assertEquals("OFF", conditionerAirMode.name)
    }
}