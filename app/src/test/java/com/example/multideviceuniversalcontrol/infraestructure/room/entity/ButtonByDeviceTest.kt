package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ButtonByDeviceTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        db = AppDatabase.createAppDataBase(context)!!
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `test get buttons by device type television`() {
        val deviceTypeTelevision = db.deviceTypeRepository().findByName(DeviceTypeConstants.TELEVISION).idDeviceType

        val buttons = db.buttonByDeviceViewRepository().findByIdDeviceType(deviceTypeTelevision)

        assertEquals(25, buttons.size)
    }

    @Test
    fun `test get buttons by device type audio`() {
        val deviceTypeTelevision = db.deviceTypeRepository().findByName(DeviceTypeConstants.AUDIO).idDeviceType

        val buttons = db.buttonByDeviceViewRepository().findByIdDeviceType(deviceTypeTelevision)

        assertEquals(6, buttons.size)
    }
}