package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class DeviceTypeTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        db = AppDatabase.createAppDataBase(context)!!
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `test quantity device types return three`() {
        val deviceTypes = db.deviceTypeRepository().getAll()

        assertEquals(3, deviceTypes.size)
    }

    @Test
    fun `test device type television`() {
        val deviceType = db.deviceTypeRepository().findByName(DeviceTypeConstants.TELEVISION)

        assertNotNull(deviceType)
        assertEquals("Television", deviceType.name)
    }

    @Test
    fun `test device type audio equipment`() {
        val deviceType = db.deviceTypeRepository().findByName(DeviceTypeConstants.AUDIO)

        assertNotNull(deviceType)
        assertEquals("Audio", deviceType.name)
    }

    @Test
    fun `test device type Conditioner Air`() {
        val deviceType = db.deviceTypeRepository().findByName(DeviceTypeConstants.AIR_CONDITIONER)

        assertNotNull(deviceType)
        assertEquals("Aire Ac.", deviceType.name)
    }
}