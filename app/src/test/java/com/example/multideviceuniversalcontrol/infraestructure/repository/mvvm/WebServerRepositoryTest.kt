package com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm

import com.example.multideviceuniversalcontrol.domain.StatusConnection
import com.example.multideviceuniversalcontrol.domain.StatusConnectionWifi
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.domain.arduino.model.StatusConnectionResponse
import com.example.multideviceuniversalcontrol.utils.ErrorEntity
import com.example.multideviceuniversalcontrol.utils.Result
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.lang.Exception

@Suppress("UNCHECKED_CAST")
class WebServerRepositoryTest {

    private val mockService = mockk<ArduinoData>()
    private val serviceRepository = WebServerRepositoryImpl(mockService)
    private lateinit var result: Result<*>
    private lateinit var statusResponse: StatusConnection

    @Test
    fun `save user id then return status success`() {
        givenSaveUserIdSuccessfully()

        whenSaveUserId()

        thenReturnStatusSuccess()
    }

    @Test
    fun `save user id fail then return error`() {
        givenSaveUserIdFail()

        whenSaveUserId()

        thenReturnError()
    }

    @Test
    fun `check connection then return status connected`() {
        givenStatusConnectionSuccessfully()

        whenCheckConnection()

        thenStatusConnectionSuccess()
    }

    @Test
    fun `check connection fail then return disconnected`() {
        givenCheckConnectionFail()

        whenCheckConnection()

        thenStatusConnectionDisconnected()
    }

    private fun givenSaveUserIdSuccessfully() {
        coEvery {
            mockService.saveUserId(any())
        } returns ResponseArduino(
            status = "success"
        )
    }

    private fun givenSaveUserIdFail() {
        coEvery {
            mockService.saveUserId(any())
        } throws Exception("algo salió mal")
    }

    private fun givenStatusConnectionSuccessfully() {
        coEvery {
            mockService.checkStatusConnection()
        } returns StatusConnectionResponse(
            status = "connected",
            isLogged = "true"
        )
    }

    private fun givenCheckConnectionFail() {
        coEvery {
            mockService.checkStatusConnection()
        } throws Exception("algo salió mal")
    }

    private fun whenSaveUserId() {
        result = runBlocking {
            serviceRepository.saveUserId("1")
        }
    }

    private fun whenCheckConnection() {
        statusResponse = runBlocking {
            serviceRepository.checkConnection()
        }
    }

    private fun thenReturnStatusSuccess() {
        assertTrue(result is Result.Success<*>)

        val response = result as Result.Success<ResponseArduino>
        Assertions.assertEquals("success", response.data.status)
    }

    private fun thenReturnError() {
        assertTrue(result is Result.Error<*>)

        val signalResponse = result as Result.Error<ErrorEntity>
        assertTrue(signalResponse.error is ErrorEntity.Unknown)
    }

    private fun thenStatusConnectionSuccess() {
        Assertions.assertEquals(StatusConnectionWifi.CONNECTED, statusResponse.status)
        assertTrue(statusResponse.isLogged)
    }

    private fun thenStatusConnectionDisconnected() {
        Assertions.assertEquals(StatusConnectionWifi.DISCONNECTED, statusResponse.status)
        assertFalse(statusResponse.isLogged)
    }

}