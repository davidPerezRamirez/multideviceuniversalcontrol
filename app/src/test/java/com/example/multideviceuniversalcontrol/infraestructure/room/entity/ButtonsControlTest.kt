package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import android.content.Context
import android.os.Build
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.example.multideviceuniversalcontrol.domain.constants.ButtonContants
import com.example.multideviceuniversalcontrol.domain.constants.DeviceTypeConstants
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.config.DeviceTypesConfig
import com.example.multideviceuniversalcontrol.domain.room.contracts.ButtonsControlDao
import com.example.multideviceuniversalcontrol.domain.room.contracts.DeviceTypeRepository
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ButtonsControlTest {

    private lateinit var buttonsControlRepository: ButtonsControlDao
    private lateinit var deviceTypesRepository: DeviceTypeRepository
    private lateinit var db: AppDatabase
    private lateinit var currentButton: ButtonsControl
    private lateinit var deviceTypes: List<DeviceType>

    @Before
    fun createDb() {
        val context = getApplicationContext<Context>()
        db = Room
            .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        buttonsControlRepository = db.buttonsControlRepository()
        deviceTypesRepository = db.deviceTypeRepository()
    }
    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.clearAllTables()
        db.close()
    }

    @Test
    fun findButtonOnFromDeviceCheckCodeIs0x20DF10EF() {
        giveThatInitializeDeviceTypeAndDeviceButtons()
        giveCodesButtonDeviceFromTV()

        whenFindDeviceButtonOnFromTV()

        assertDeviceButtonOnIs0x20DF10EF()
        assertDeviceNameIsTelevision()
    }

    private fun giveThatInitializeDeviceTypeAndDeviceButtons() {
        val deviceTypes = arrayListOf(
            DeviceType(name = DeviceTypeConstants.TELEVISION)
        )
        val deviceButtons = arrayListOf(
            DeviceButton(name = ButtonContants.ON_OFF)
        )
        val devices = arrayListOf(
            Device(idDeviceType = 1, name = "Tv Comedor")
        )

        db.deviceTypeRepository().insertAll(deviceTypes = deviceTypes)
        db.deviceButtonRepository().insertAll(deviceButtons = deviceButtons)
        db.deviceRepository().insertAll(devices)
    }

    @Test
    fun checkDefaultDeviceTypesContainsTelevision() {
        giveThatLoadDefaultDevices()

        whenGetAllTypeDevices()

        assertExistDeviceTypeTelevision()
    }

    @Test
    fun checkDefaultDeviceTypesContainsAudioSystem() {
        giveThatLoadDefaultDevices()

        whenGetAllTypeDevices()

        assertExistDeviceTypeAudioSystem()
    }

    @Test
    fun checkDefaultDeviceTypesContainsAcontionedAir() {
        giveThatLoadDefaultDevices()

        whenGetAllTypeDevices()

        assertExistDeviceTypeAireAcontioned()
    }

    private fun giveThatLoadDefaultDevices() {
        val deviceTypes = DeviceTypesConfig.create()

        deviceTypesRepository.insertAll(deviceTypes = deviceTypes)
    }

    private fun giveCodesButtonDeviceFromTV() {
        val buttonTypeDeviceRelationRepository = db.buttonTypeDeviceRelationRepository()

        val buttonTypeDeviceRelations = arrayListOf(
            ButtonTypeDeviceRelation(idDevice = 1, idDeviceButton = 1, code = "0x20DF10EF")
        )

        buttonTypeDeviceRelationRepository.insertAll(buttonTypeDeviceRelations)
    }

    private fun whenFindDeviceButtonOnFromTV() {
        currentButton = buttonsControlRepository.findButtonFromDevice(1, ButtonContants.ON_OFF)!!
    }

    private fun whenGetAllTypeDevices() {
        deviceTypes = deviceTypesRepository.getAll()
    }

    private fun assertExistDeviceTypeTelevision() {
        assert(deviceTypes.any{device -> device.name == "Television"})
    }

    private fun assertExistDeviceTypeAudioSystem() {
        assert(deviceTypes.any{device -> device.name == "Audio"})
    }

    private fun assertExistDeviceTypeAireAcontioned() {
        assert(deviceTypes.any{device -> device.name == "Aire Ac."})
    }

    private fun assertDeviceButtonOnIs0x20DF10EF() {
        assertThat(currentButton.code, `is`(equalTo("0x20DF10EF")))
    }

    private fun assertDeviceNameIsTelevision() {
        assertThat(currentButton.idDevice, `is`(equalTo(1)))
    }

}
