package com.example.multideviceuniversalcontrol.infraestructure.repository

import android.content.SharedPreferences
import com.example.multideviceuniversalcontrol.infraestructure.repository.PreferencesRepository
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys.AIR_COND_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesKeys.AIR_COND_TEMP
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Test

class PreferencesRepositoryTest {

    private val mockSharedPreferences = mockk<SharedPreferences>()
    private val mockSharedPreferencesEditor = mockk<SharedPreferences.Editor>()
    private lateinit var preferencesRepository: PreferencesRepository

    @Before
    fun setUp() {
        preferencesRepository = PreferencesRepository(mockSharedPreferences)

        every { mockSharedPreferences.edit() } returns mockSharedPreferencesEditor
        every { mockSharedPreferencesEditor.putInt(any(), any()) } returns mockSharedPreferencesEditor
        every { mockSharedPreferencesEditor.putString(any(), any()) } returns mockSharedPreferencesEditor
        justRun { mockSharedPreferencesEditor.apply() }
    }

    @Test
    fun saveIntValuePreferencesThenSaveValue24WithIdCurrentTemperature() {

        whenSavePreferenceValue24WithIdAirCondTemperature()

        thenSaveIntValue24WithIdAirCondTemperature()
    }

    @Test
    fun saveStringValuePreferencesThenSaveValueFrioWithIdAirCondMode() {

        whenSavePreferenceValueFrioWithIdAirCondMode()

        thenSaveStringValueFrioWithIdAirCondMode()
    }

    private fun whenSavePreferenceValueFrioWithIdAirCondMode() {
        this.preferencesRepository.save(AIR_COND_MODE, "Frío")
    }

    private fun whenSavePreferenceValue24WithIdAirCondTemperature() {
        this.preferencesRepository.save(AIR_COND_TEMP, 24)
    }

    private fun thenSaveIntValue24WithIdAirCondTemperature() {
        verify(exactly = 1) { mockSharedPreferences.edit() }
        verify(exactly = 1) { mockSharedPreferencesEditor.putInt(AIR_COND_TEMP, 24) }
        verify(exactly = 1) { mockSharedPreferencesEditor.apply() }
    }

    private fun thenSaveStringValueFrioWithIdAirCondMode() {
        verify(exactly = 1) { mockSharedPreferences.edit() }
        verify(exactly = 1) { mockSharedPreferencesEditor.putString(AIR_COND_MODE, "Frío") }
        verify(exactly = 1) { mockSharedPreferencesEditor.apply() }
    }

}