package com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm

import com.example.multideviceuniversalcontrol.domain.SendNECSignalResponse
import com.example.multideviceuniversalcontrol.domain.SendSignalStatus
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ButtonsControl
import com.example.multideviceuniversalcontrol.utils.ErrorEntity
import com.example.multideviceuniversalcontrol.utils.Result
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@Suppress("UNCHECKED_CAST")
class DeviceRepositoryTest {

    private val mockService = mockk<ArduinoData>()
    private val mockFirebaseService = mockk<FirebaseService>()
    private val mockDataBase = mockk<AppDatabase>()
    private val repository = DeviceRepositoryImpl(mockService, mockFirebaseService)
    private lateinit var result: Result<*>

    @BeforeEach
    fun setUp() {
        AppDatabase.INSTANCE = mockDataBase
    }

    @Test
    fun `send signal the return status success`() {
        givenValidCodeToSend()
        givenServiceResponseSuccessfully()

        whenSendSignal()

        thenReturnStatusSuccess()
    }

    @Test
    fun `send signal but code not exist the return status not found`() {
        givenCodeNotExist()
        givenServiceResponseSuccessfully()

        whenSendSignal()

        thenReturnStatusNotFound()
    }

    @Test
    fun `send signal fail the return error`() {
        givenValidCodeToSend()

        whenSendSignal()

        thenReturnError()
    }

    @Test
    fun `save code in cloud`() {
        givenValidCodeToSend()
        givenFirebaseResponseSuccessfully()

        runBlocking{
            repository.saveCodeInCloud({}, 1, "Apagado/Encendido")
        }

        thenSaveCode()
    }

    private fun givenValidCodeToSend() {
        every {
            mockDataBase.buttonsControlRepository().findButtonFromDevice(any(), any())
        } returns ButtonsControl(
            idDevice = 1,
            nameButton = "Apagado/Encendido",
            code = listOf(1, 2, 3, 4).toString()
        )
    }

    private fun givenServiceResponseSuccessfully() {
        coEvery {
            mockService.sendNEC(any())
        } returns ResponseArduino(status = "success")
    }

    private fun givenCodeNotExist() {
        every {
            mockDataBase.buttonsControlRepository().findButtonFromDevice(any(), any())
        } returns null
    }

    private fun givenFirebaseResponseSuccessfully() {
        coJustRun {
            mockFirebaseService.saveCode(any(), any())
        }
    }

    private fun whenSendSignal() {
        result = runBlocking {
            repository.sendSignal(1, "Apagado/Encendido")
        }
    }

    private fun thenReturnStatusSuccess() {
        Assertions.assertTrue(result is Result.Success<*>)

        val signalResponse = result as Result.Success<SendNECSignalResponse>
        Assertions.assertEquals(SendSignalStatus.SUCCESS, signalResponse.data.status)
    }

    private fun thenReturnStatusNotFound() {
        Assertions.assertTrue(result is Result.Success<*>)

        val signalResponse = result as Result.Success<SendNECSignalResponse>
        Assertions.assertEquals(SendSignalStatus.NOT_FOUND, signalResponse.data.status)
    }

    private fun thenReturnError() {
        Assertions.assertTrue(result is Result.Error<*>)

        val signalResponse = result as Result.Error<ErrorEntity>
        Assertions.assertTrue(signalResponse.error is ErrorEntity.Unknown)
    }

    private fun thenSaveCode() {
        coVerify(exactly = 1) {
            mockFirebaseService.saveCode(any(), any())
        }
    }

}