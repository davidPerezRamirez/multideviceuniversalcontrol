package com.example.multideviceuniversalcontrol.infraestructure.repository.mvvm

import com.example.multideviceuniversalcontrol.domain.SendRawSignalResponse
import com.example.multideviceuniversalcontrol.domain.SendSignalStatus
import com.example.multideviceuniversalcontrol.utils.Result
import com.example.multideviceuniversalcontrol.domain.arduino.ArduinoData
import com.example.multideviceuniversalcontrol.domain.arduino.model.ResponseArduino
import com.example.multideviceuniversalcontrol.infraestructure.FirebaseService
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import com.example.multideviceuniversalcontrol.infraestructure.room.entity.ConditionerAirButtonControl
import com.example.multideviceuniversalcontrol.utils.ErrorEntity
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@Suppress("UNCHECKED_CAST")
class ConditionerAirRepositoryTest {

    private val mockService = mockk<ArduinoData>()
    private val mockFirebaseService = mockk<FirebaseService>()
    private val mockDataBase = mockk<AppDatabase>()
    private val repository = ConditionerAirRepositoryImpl(mockService, mockFirebaseService)
    private lateinit var result: Result<*>

    @BeforeEach
    fun setUp() {
        AppDatabase.INSTANCE = mockDataBase
    }

    @Test
    fun `send conditioner air signal the return status success`() {
        givenValidCodeToSend()
        givenServiceResponseSuccessfully()

        whenSendConditionerAirSignal()

        thenReturnStatusSuccess()
    }

    @Test
    fun `send conditioner air signal but code not exist the return status not found`() {
        givenCodeNotExist()
        givenServiceResponseSuccessfully()

        whenSendConditionerAirSignal()

        thenReturnStatusNotFound()
    }

    @Test
    fun `send conditioner air signal fail the return error`() {
        givenValidCodeToSend()

        whenSendConditionerAirSignal()

        thenReturnError()
    }

    @Test
    fun `send conditioner air off signal`() {
        givenValidOffCodeToSend()
        givenServiceResponseSuccessfully()

        whenSendConditionerAirOffSignal()

        thenSendOffSignal()
    }

    @Test
    fun `send conditioner air off signal but code not exist the return status not found`() {
        givenCodeOffNotExist()
        givenServiceResponseSuccessfully()

        whenSendConditionerAirOffSignal()

        thenReturnStatusNotFound()
    }

    @Test
    fun `send conditioner air off signal fail the return error`() {
        givenValidOffCodeToSend()

        whenSendConditionerAirOffSignal()

        thenReturnError()
    }

    @Test
    fun `save code in cloud`() {
        givenValidCodeToSend()
        givenFirebaseResponseSuccessfully()

        runBlocking{
            repository.saveCodeInCloud({}, 1, "heat", 27, "normal")
        }

        thenSaveCode()
    }

    @Test
    fun `save off code in cloud`() {
        givenValidOffCodeToSend()
        givenFirebaseResponseSuccessfully()

        runBlocking{
            repository.saveCodeInCloud({}, 1)
        }

        thenSaveCode()
    }

    private fun givenValidCodeToSend() {
        every {
            mockDataBase.conditionerAirButtonsControl().findButton(any(), any(), any(), any())
        } returns ConditionerAirButtonControl(
            idDevice = 1,
            mode = "heat",
            temperature = 27,
            speed = "normal",
            code = listOf(1, 2, 3, 4).toString()
        )
    }

    private fun givenValidOffCodeToSend() {
        every {
            mockDataBase.conditionerAirButtonsControl().findOffButton(any())
        } returns ConditionerAirButtonControl(
            idDevice = 1,
            mode = "heat",
            temperature = 27,
            speed = "normal",
            code = listOf(1, 2, 3, 4).toString()
        )
    }

    private fun givenCodeNotExist() {
        every {
            mockDataBase.conditionerAirButtonsControl().findButton(any(), any(), any(), any())
        } returns null
    }

    private fun givenCodeOffNotExist() {
        every {
            mockDataBase.conditionerAirButtonsControl().findOffButton(any())
        } returns null
    }

    private fun givenServiceResponseSuccessfully() {
        coEvery {
            mockService.sendRaw(any())
        } returns ResponseArduino(status = "success")
    }

    private fun givenFirebaseResponseSuccessfully() {
        coJustRun {
            mockFirebaseService.saveCode(any(), any())
        }
    }

    private fun whenSendConditionerAirSignal() {
        result = runBlocking {
            repository.sendConditionerAirSignal(1, "heat", 27, "normal")
        }
    }

    private fun whenSendConditionerAirOffSignal() {
        result = runBlocking {
            repository.sendConditionerAirOffSignal(1)
        }
    }

    private fun thenReturnStatusSuccess() {
        assertTrue(result is Result.Success<*>)

        val signalResponse = result as Result.Success<SendRawSignalResponse>
        assertEquals(SendSignalStatus.SUCCESS, signalResponse.data.status)
        assertFalse(signalResponse.data.isOffButton)
    }

    private fun thenSendOffSignal() {
        assertTrue(result is Result.Success<*>)

        val signalResponse = result as Result.Success<SendRawSignalResponse>
        assertEquals(SendSignalStatus.SUCCESS, signalResponse.data.status)
        assertTrue(signalResponse.data.isOffButton)
    }

    private fun thenReturnStatusNotFound() {
        assertTrue(result is Result.Success<*>)

        val signalResponse = result as Result.Success<SendRawSignalResponse>
        assertEquals(SendSignalStatus.NOT_FOUND, signalResponse.data.status)
    }

    private fun thenReturnError() {
        assertTrue(result is Result.Error<*>)

        val signalResponse = result as Result.Error<ErrorEntity>
        assertTrue(signalResponse.error is ErrorEntity.Unknown)
    }

    private fun thenSaveCode() {
        coVerify(exactly = 1) {
            mockFirebaseService.saveCode(any(), any())
        }
    }

}