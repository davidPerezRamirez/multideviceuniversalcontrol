package com.example.multideviceuniversalcontrol.infraestructure.room.entity

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.COOL_MODE
import com.example.multideviceuniversalcontrol.domain.constants.PreferencesValues.HEAT_MODE
import com.example.multideviceuniversalcontrol.infraestructure.room.AppDatabase
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ConditionerAirTemperatureTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        db = AppDatabase.createAppDataBase(context)!!
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun `test quantity conditioner air temperatures return 14`() {
        val conditionerAirModes = db.conditionerAirTemperatureRepository().getAll()

        assertEquals(14, conditionerAirModes.size)
    }

    @Test
    fun `test conditioner air temperatures 19 to 30`() {
        val conditionerAirTemperatures = db.conditionerAirTemperatureRepository().getAll()
        var currentTemperature = 17

        conditionerAirTemperatures.forEach { conditionerAirTemperature ->
            assertEquals(currentTemperature, conditionerAirTemperature.value)

            currentTemperature++
        }
    }
}